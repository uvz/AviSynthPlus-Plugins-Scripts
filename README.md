### New versions [here](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts1)

### Avisynth+ plugins pack (after plugins_pack_r65 only x64 plugins)

#### Content of this plugins pack - [here](https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts/-/blob/master/Filters.md)

### Requirements

- [The latest version of this AIO pack](https://github.com/abbodi1406/vcredist/releases)
- [Git](https://git-scm.com/)
- PowerShell (only for LinearTransformation)
- Recent AviSynthPlus version (can be found [here](https://gitlab.com/uvz/AviSynthPlus-Builds) or [here](https://forum.doom9.org/showthread.php?t=181351))

#### How to use this plugins pack for the first time

1. Make backup of the plugins folder you already have.
2. Clone this repo: `git clone https://gitlab.com/uvz/AviSynthPlus-Plugins-Scripts`\
a) If the location of the cloned repo will be used as default location for Avisynth plugins, run `create_symlinks.bat` as Administrator.\
b) If the default Avisynth plugins location is different than the one of the cloned repo (for example "C:\Program Files (x86)\AviSynth+"), use the included plugins_updater.bat.
	- Open `plugins_updater.bat` and change the relevant settings if needed.
	- Run `plugins_updater.bat` and choose what version of the plugins to update (don't forget to run it as Administrator).
3. Use [Avisynth Info Tool](https://forum.doom9.org/showthread.php?t=176079) to check if everything is ok. There could be warning for duplicated functions - it could be safely ignored.

#### How to update this plugins pack (it's assumed that the repo is already cloned)

1) Update the files: git pull
2) Run again create_symlinks.bat / plugins_updater.bat.

#### Note
This repo is used solely for archiving and keeping various plugins in one place.
