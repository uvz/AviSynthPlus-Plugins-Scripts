##

- Updated VSFilter - 3.2.0.810.
- Updated mlrt_ncnn - 1.0.2.
- Updated mlrt_ov - 1.0.1.
- Updated mlrt.avsi - 1.1.3.
- Updated RIFE - 1.1.0.

## plugins_pack_r72

- Updated L-SMASH-Works - 1167.0.0.0.
- Updated manyPlus - 30.06.2023.
- Updated eedi3_resize16.avsi - v3.3.16.
- Updated QTGMC.avsi - 3.385.
- Updated VideoTek.avsi - v1.6.
- Updated VSFilter - 3.2.0.809.
- Updated SoxFilter - 2.2.0.
- Updated TemporalDegrain2.avsi - v2.6.7.
- Added [MaxCLLFindAVS](https://github.com/erazortt/MaxCLLFindAVS) - v0.36.
- Updated ffms2/ffmsindex - r1393.

## plugins_pack_r71

- Updated BWDIF - 1.3.0.
- Updated LinearTransformation - 2.6.
- Updated L-SMASH-Works - 1160.0.0.0.
- Updated DoViBaker - [0acf6b7](https://github.com/Asd-g/DoViBaker).
- Added [MapNLQ](https://github.com/Asd-g/AviSynthPlus-MapNLQ) - 1.0.0.
- Updated Dogway_pack - 24432b3.
- Added [DeScratch](http://avisynth.nl/index.php/DeScratch) - r1.
- Updated avs_libplacebo - 1.5.2.
- Updated ffms2/ffmsindex - r1391.
- Updated ssimulacra - 1.1.0.1.
- Updated ResampleMT - 2.3.9.
- Updated NNEDI3 - 0.9.4.63.
- Updated HDRTools - 1.0.5.
- Updated aWarpsharpMT - 2.1.9.
- Updated Delay_.avsi - 1.0.1.
- Updated ComparisonGen.avsi - 1.0.2.
- Updated AutoResize_.avsi - 1.1.1.
- Updated DGDecNV - 251.
- Updated DGIndex - 2.0.0.8.
- Updated GrainStabilizeMC.avsi - 2023.12.01.
- Added [SoxFilter](https://github.com/pinterf/SoxFilter) - 2.1.0.
- Updated AutoOverlay - 0.6.1.
- Added [SoftLight](https://forum.doom9.org/showthread.php?t=184943) - v1.11.
- Updated RawSourcePlus - 1.3.3.
- Updated TDeint - v1.9.
- Updated TIVTC - v1.0.28.

## plugins_pack_r70

- Updated AnimeIVTC.avsi - v2.389.
- Updated avs_libplacebo - 1.4.1.
- Updated NNEDI3CL - 1.0.8.
- Updated NNEDI3CL_rpow2.avsi.
- Updated ssimulacra - 1.1.0.
- Updated ffms2/ffmsindex - r1369.
- Updated RIFE - 1.0.4.
- Updated w2xncnnvk - 1.0.2.
- Updated Dogway_pack - 9b0dcf5.
- Updated avsresize - r23.
- Updated L-SMASH-Works - 1129.0.1.0.
- Updated vsTTempSmooth - 1.2.6.
- Updated EEDI3CL - 1.0.2.
- Updated yadifmod2 - 0.2.8.
- Updated Butteraugli - 2.0.2.
- Updated SangNom2 - 0.6.1.
- Added [ExBlend](https://forum.doom9.org/showthread.php?t=175948) - v1.04.
- Updated InpaintDelogo.avsi - v3.7.
- Updated vscube - 12.07.2023.
- Updated ExTools.avsi - v10.3.
- Updated LinearTransformation.avsi - 2.4.

## plugins_pack_r69

- Updated dither - 1.28.1.1: updated to 2.6 plugin, added support for passthrough frame properties.
- updated Dogway_pack - aebb92d.
- Updated VideoTek.avsi - v1.5.
- Updated ffms2/ffmsindex - r1360.
- Updated DGDecNV - 248.
- Updated LWLInfo.avsi - added add_text (additional text) and add_text_pos (top or bottom position of the additional text) parameters.
- Updated FillBorders - 1.4.1.
- Updated bborders.avsi - changed the behavior of cTop/cBottom/cLeft/cRight values when specified for chroma planes - previously the values were right bit shifted by the subsampling factor (for example, YUV 4:2:0 and cTop=[2,4] means 2px for chroma planes will be filtered - 4 >> 1 (chroma subsampling)); now the values are passed as specified; changed the required AviSynth+ version; fixed processing when the input has one plane.
- Updated mlrt.avsi - 1.1.2.
- Updated L-SMASH-Works - 20230611.
- Added [DoomDelogo.avsi](https://github.com/Purfview/DoomDelogo) - 1.0.
- Added [AudioGraph.dll](https://github.com/Asd-g/AviSynth-AudioGraph) - v0.0.2.
- Added [BlurDetect](https://github.com/Asd-g/AviSynthPlus-BlurDetect/releases) - 1.0.0.

## plugins_pack_r68

- Added [Brightness.avsi](https://forum.doom9.org/showthread.php?p=1936224#post1936224) - v1.2.
- Updated avsresize - r21.
- Updated ffms2/ffmsindex - ad42af1.
- Updated Dogway_pack - d7fbae5.
- Updated LWLInfo.avsi - added info about prophoto transfer and primaries.
- Updated ExTools.avsi - v10.2.
- Updated L-SMASH-Works - 20230220.
- Updated DoviBaker - 8396200.
- Updated autoLoadDLLs - removed dovi.dll.
- Updated mlrt.avsi - 1.1.1.
- Updated TemporalDegrain2.avsi - v2.6.6.
- Updated aWarpsharpMT - 2.1.8.
- Updated HDRTools - 1.0.4.
- Updated NNEDI3 - 0.9.4.62.
- Updated ResampleMT - 2.3.8.
- Updated RIFE - 1.0.3.
- Updated vsTTempSmooth - 1.2.3.
- Added [ddcr.avsi](http://avisynth.nl/index.php/DDCR) - v1.0.0.
- Added [NNEDI3CL_rpow2.avsi](https://github.com/Asd-g/AviSynthPlus-NNEDI3CL/blob/main/NNEDI3CL_rpow2.avsi) - 1.0.3.
- Updated libdescale - 8c53f5d.
- Updated avs_libplacebo - 1.2.0.
- Updated NNEDI3CL - 1.0.7.
- Updated EEDI3CL - 1.0.1.
- Updated FixBrightnessProtect3.avsi - changed required AviSynth+ version; code cleaned; 32-bit clips are assumed within range [0, 1] now.
- Updated mlrt_ncnn - 1.0.1.
- Added [mlrt_ov](https://github.com/Asd-g/avs-mlrt/releases) - 1.0.0.
- Updated VideoTek.avsi - a920441.
- Updated AnimeIVTC.avsi - v2.386 2023-03-20 mod.
- Updated Zs_RF_Shared.avsi - V1.161.
- Updated eedi3_resize16.avsi - v3.3.15 2023.03.27.
- Updated nnedi3_resize16.avsi - v3.3 2023.03.27.
- Updated Santiag.avsi - v1.607.
- Added [BlockDetect](https://github.com/Asd-g/AviSynthPlus-BlockDetect/releases) - 1.0.1.

## plugins_pack_r67

- Updated Butteraugli - 2.0.1.
- Updated Dogway_pack - 8e11f47.
- Updated ReplaceFrameX.avsi - 2022-12-17.
- Updated LinearTransformation - 6bf4611.
- Added [f3kgrainplus.avsi](http://avisynth.nl/index.php/F3kgrainPlus).
- Updated VSFilter - 3.2.0.808.
- Added [ssimulacra](https://github.com/Asd-g/AviSynthPlus-ssimulacra) - 1.0.0.
- Updated ExTools.avsi - v9.9.
- Updated neo_f3kdb - r9.
- Updated JincResize - 2.1.2.
- Updated avs_libplacebo - 1.1.5.
- Updated InpaintDelogo.avsi - 3.6.
- Updated bborders.avsi - added note that only progressive video is supported; improved some error messages.
- Updated DGDecNV - 246.
- Updated gradation - r74.
- Updated BM3D - test10.
- Added [avs-mlrt](https://github.com/Asd-g/avs-mlrt/releases) - 1.0.0.
- Updated BWDIF - 1.2.5.
- Updated DoviBaker - e1f1176.
- Updated avsresize - r20.
- Updated ffms2/ffmsindex - 41af11c.
- Updated Resize8.avsi - 2023-02-09.
- Added [TemporalDegrain2_fast.avsi](https://forum.doom9.org/showthread.php?p=1982594#post1982594).

## plugins_pack_r66

- Updated w2xncnnvk - 1.0.1.
- Updated ExTools.avsi - v9.3.
- Updated Dogway_pack - @139be5e.
- Updated RIFE - 1.0.1.
- Updated NNEDI3CL - 1.0.6.
- Updated DGCube - 22/09/30.
- Updated avs_libplacebo - 1.1.3.
- Updated DPID - 1.1.0.
- Updated Resize8.avsi - 2022-09-23.
- Removed Hqdn3dY.
- Updated MCTD_.avsi - v1.4.20 mod4.4.
- Updated DoviBaker - @9afba6f.
- Updated LinearTransformation - @1669b55.
- Updated RIFE - 1.0.2.
- Updated VMAF - 2.1.2.
- Updated InpaintDelogo.avsi - 3.4.
- Added [ReplaceFrameX.avsi](https://forum.doom9.org/showthread.php?p=1976553#post1976553) - 2022-10-16.
- Updated Drawrect_.avsi - [0.10](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/Drawrect_.avsi).
- Updated LSFmod.avsi - @1d14c52.
- Updated QTGMC.avsi - @113f964.
- Updated OverlayPlus.avsi - v1.3.0.
- Updated debandmask_.avsi - added parameter onlyY.
- Updated BWDIF - 1.2.4.
- Updated MyLWLInfo.avsi - removed the dependency of grunt.
- Updated L-SMASH-Works - 20221109.
- Updated waveform - 2.0.2.
- Updated HDRTools - 1.0.3.
- Updated ResampleMT - 2.3.6.
- Updated aWarpsharpMT - 2.1.7.
- Updated NNEDI3 - 0.9.4.61.
- Updated AutoResize_.avsi - 1.0.0.
- Updated UpscaleCheck.avsi - 1.0.1.
- Updated ChromaShiftSP2_.avsi - fixed incorrect shifting; also named the resizer parameters so it does not error out if "bicubic" is specified and some cosmetics.
- Added [EEDI3CL](https://github.com/Asd-g/AviSynthPlus-EEDI3CL) - 1.0.0.
- Added [WDFPlus.avsi](http://avisynth.nl/index.php/WDFPlus) - 1.0.0.

## plugins_pack_r65

- Updated Dogway_pack - @e50ea2b.
- Added [w2xncnnvk](https://github.com/Asd-g/AviSynthPlus-w2xncnnvk/releases) - 1.0.0.
- Updated avsresize - r19.
- Updated DGCube.
- Added [RIFE](https://github.com/Asd-g/AviSynthPlus-RIFE/releases) - 1.0.0.
- Added [BlindPP](https://www.rationalqm.us/dgmpgdec/DGDecodeManual.html#BlindPP) - 0.2.0.
- Updated LinearTransformation - @77c4c1f.
- Removed AutoYUY2.
- Updated AviSynthPluginsDir.avsi.
- Updated ExTools.avsi - 8.8.
- Updated VideoTek.avsi.
- Updated neo_DFTTest - r8.
- Added [DoViBaker](https://github.com/Asd-g/DoViBaker) - @072ea06 with additional parameter `outYUV`and some other fixes.
- Updated fmtconv - r30.
- Updated ComparisonGen.avsi - 1.0.1.
- Removed GradFun2db.
- Removed GrainFactory3.avsi - use GrainFactory3mod.avsi.
- Removed Info2.
- Removed MasksPack.avsi.
- Updated mtmodes.avsi.
- Updated DeHalo_alpha_MT2.avsi - @4bf0b83.
- Removed sync_tools.avsi.
- Updated UpscaleCheck.avsi - 1.0.0.
- Added [f3kpf.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/f3kpf.avsi) - 1.0.0.
- Added [lfdeband.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/lfdeband.avsi) - 1.0.1.
- Updated avs_libplacebo - 1.1.0.
- Updated neo_f3kdb - r8.

## plugins_pack_r64

- Updated InpaintDelogo.avsi - v2.17.
- Updated RoundHalfToEven.avsi - fixed check for negative numbers.
- Updated AnimeMask2.avs - removed redundant code.
- Updated AutoResize_.avsi - removed redundant code.
- Updated IQA_downsample.avsi - removed redundant code.
- Updated MDSI.avsi - removed redundant code.
- Updated bt2390_pq.avsi - removed redundant code.
- Removed GradePack_.avsi.
- Added Dogway_pack.
- Updated TemporalDegrain2.avsi - v2.6.3.
- Added [tcolormask](https://github.com/Asd-g/AviSynth-tcolormask/releases) - 1.2.2.
- Updated SmoothUV2 - 4.4.1.
- Updated vsCnr2 - 1.0.1.
- Updated HDRTools - 1.0.1.
- Updated ComparisonGen.avsi - 1.0.0.
- Added [NNEDI3CL](https://github.com/Asd-g/AviSynthPlus-NNEDI3CL/releases) - 1.0.3.
- Updated Frfun7  - 0.9.
- Updated BM3D - test9.
- Added [avs_libplacebo](https://github.com/Asd-g/avslibplacebo/releases) - 1.0.1.
- Updated libdescale - @fcd73b2.
- Updated Retinex - 1.0.3.
- Updated band_det - added threshold values for different resolutions; changed behavior when cambi metrics are 0.
- Updated RawSourcePlus - 1.3.2.
- Updated avsresize - r18.
- Updated debandmask_.avsi - added paramater mask.
- Updated MTCombMask - 2.2.1.
- Updated LinearTransformation.
- Updated DGDecNV - 245.
- Added DGCube.

## plugins_pack_r63

- Updated ExTools.avsi - v8.5.
- Updated vinverse - 0.9.4.
- Updated QTGMC.avsi - 3.384.
- Updated AnimeIVTC.avsi - v2.386.
- Updated autodeblock2.avsi - 1.21.
- Removed Vinverse_avsi.avsi.
- Updated vsTCanny - 1.1.8.
- Added [grayworld](https://github.com/Asd-g/AviSynthPlus-grayworld/releases) - 1.02.
- Added [libdescale](https://github.com/Irrational-Encoding-Wizardry/descale/releases) - r8.
- Added [gradation](https://github.com/magiblot/gradation/releases) - r51.
- Updated InpaintDelogo.avsi - v2.14.
- Updated L-SMASH-Works - 20220505.
- Updated LinearTransformation - BT709 SDR to BT2020 HLG.
- Updated eedi3_resize16.avsi - v3.3.14.
- Updated fmtconv - r29.
- Updated avsresize - r14.
- Updated ffms2/ffmsindex - ffms2_6ad7738.
- Updated D2VSource - 1.3.0.
- Updated VMAF - 2.1.1.
- Added [sbr](https://github.com/Asd-g/AviSynth-sbr/releases) - 1.0.0.
- Added [crabshank_filters](https://github.com/crabshank/Avisynth-filters) -r1132; they can be omitted with the option in `plugins_updater.bat`.
- Added [MosquitoNR](https://github.com/pinterf/MosquitoNR/releases/tag/v0.3) - v0.3.
- Updated Zs_RF_Shared.avsi - V1.159.
- Updated GrainStabilizeMC.avsi - replaced sbr with sbr_avsi.
- Updated TemporalDegrain2.avsi - v2.6.2.
- Updated edgelevel - 0.04.
- Added [AGM](https://github.com/Asd-g/AviSynth-AGM/releases) - 1.0.0.
- Removed AdaptiveGrain.avsi.
- Updated LWLInfo.avsi - added new D2VSource frame properties (_FieldOperation, _FieldOrder, _Film, _ProgressiveFrame, _RFF, _TFF); updated _AspectRatio type for the latest D2VSource; fixed missed default values.
- Updated Frfun7 - v0.8.
- Updated Tonemapper.avsi - v1.0.
- ComparisonGen.avsi - allowed clips with different length.
- Updated bt2390_pq.avsi - changed default values of tdmaxl, cs, use_alternate and colors.
- Updated MyLWLInfo.avsi - added check for existing of _PictType frame property.
- Updated vsTEdgeMask - 1.0.1.

## plugins_pack_r62

- Updated hqdn3d.
- Updated vsTCanny.
- Removed GradFun2DBmod.avsi.
- Added [GradFun3DBmod.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/GradFun3DBmod.avsi).
- Added [ExTools](https://github.com/Dogway/Avisynth-Scripts/blob/master/ExTools.avsi).
- Updated MCTD_.avsi - fixed the high bit depth processing; replaced GradFun2DBmod with GradFun3DBmod.
- Updated oSmoothLevels.avsi and file name changed to oSmoothLevels_.avsi - replaced GradFun2* filters with GradFun3.
- Updated TimecodeFPS.
- Updated FixBrightnessProtect3.avsi - fixed nuked _ChromaLocation frame property if available in the source.
- Updated bborders.avsi - fixed nuked _ChromaLocation frame property if available in the source.
- Updated bbmodY.avsi - fixed nuked _ChromaLocation frame property if available in the source; removed duplicated code; added restriction for using column and row simultaneously; allowed column, row, cTop, cBottom, cLeft, cRight to be specified 0 (for example bbmodY(10, 0, 0, 0, 0, 0, 3, 0, 100, 300)); restrictred the usage of cTop, cBottom, cLeft, cRight - only one of them can be greater than 0 or array; fixed column/row = 0.
- Upated vscube.
- Updated Resize8.avsi.
- Added [Pixelscope](http://avisynth.nl/index.php/Pixelscope).
- Updated LWLInfo.avsi - allowed user defined runtime variables.
- Updated FFMS2_.avsi - removed global variables; fixed default value of y when align 4/5/6.
- Added [edgelevel](http://avisynth.nl/index.php/EdgeLevel).
- Updated waveform.
- Updated spline36resizemod_.avsi - added parameter "threads".
- Updated Retinex.
- Updated dldet.avsi - changed the type of left, top, right, bottom; added parameter n and prefilter; removed w and h; replaced tl, tt, tr, tb with thr, thr1; disabled prefiltered video with prefilter="".
- Updated LinesLumaDiff.
- Updated [vinverse](https://github.com/Asd-g/vinverse/releases).
- Added [LinearTransformation.avsi](https://github.com/FranceBB/LinearTransformation).
- Updated AnimeIVTC.avsi.
- Updated F3KDB_s.avsi.
- Updated Santiag.avsi.
- Updated Vinverse_avsi.avsi.
- Updated YAHR (YAHR.avs and YAHRmod.avsi).
- Updated Zs_RF_Shared.avsi.
- Updated autodeblock2.avsi.
- Updated edi_rpow2.avsi.
- Updated eedi3_resize16.avsi.
- Updated nnedi3_resize16.avsi.
- Added [STGMC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/STGMC.avsi).
- Updated QTGMC.avsi.
- Updated AddGrainC.
- Removed addgrain.avsi.
- Updated AutoOverlay.
- Updated aWarpsharpMT.
- Updated BM3D.
- Updated HDRTools.
- Updated InpaintDelogo.avsi.
- Updated manyPlus.
- Updated masktools2.
- Updated nnedi3.
- Updated ResampleMT.
- Updated OverlayUtils.avsi.

## plugins_pack_r61

- Updated ffms2/ffmsindex - ffms2@d0e9dcb; ffmpeg@1bc1757; dav1d 0.9.1; zlib 1.2.11; capped threads to 2 for VP9.
- Added [PropToClip](https://github.com/Asd-g/AviSynthPlus-PropToClip/releases).
- Added [SubImageFile](https://github.com/Asd-g/AviSynthPlus-SubImageFile/releases).
- Added [BlendSubtitles.avsi](https://github.com/Asd-g/AviSynthPlus-SubImageFile/blob/1.0.0/BlendSubtitles.avsi).
- Removed SupT.avsi, SupCore, SupTitle.
- Updated VMAF.
- Updated ComparisonGen.avsi - changed default values of image_file; changed image_file type from val to string_array; changed frame_type from val to val_array.
- Updated avsresize - added parameter use_props; zimg 3.0.3.
- Updated retinex_edgemask.avsi.
- Removed avstp.
- Moved balanceborders_.avsi and bbmod.avs into one file bborders.avsi.
- Removed prewitt.avsi.
- Updated D2VSource.
- Updated vsTCanny.
- Updated hqdn3d.
- Removed Kirsch.avsi.
- Updated Zs_RF_Shared.avsi - updated for the latest vsTCanny.
- Updated masktools2.
- Updated AvsInpaint.
- Updated dfttest.
- Updated fmtcavs (file name changed to fmtconv).
- Updated DGHDRtoSDR.
- Updated InpaintDelogo.avsi.
- Updated AnimeIVTC.avsi.
- Updated GradationCurve.
- Updated TemporalDegrain2.avsi.
- Updated QTGMC.avsi.*

## plugins_pack_r60

- Updated dldet.avsi - fixed right, bottom when debug=true; changed default values for threshold; added parameter show.
- Updated bbmodY.avsi - better wording.
- Updated vsTCanny.
- Updated SSIM_downsample.avsi - fixed smooth when it's float.
- Updated AutoResize_.avsi - added support for some kernels that don't have parameters src_left/src_top/src_width/src_height: DPID, DPIDraw, fmtc_resample, ResizeShader, SSIM_downsample.
- Updated JincResize.
- Updated DeDot.
- Added [Retinex](https://github.com/Asd-g/AviSynth-Retinex/releases).
- Added [retinex_edgemask.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/retinex_edgemask.avsi).
- Updated neo_TMedian.
- Updated neo_minideen.
- Updated timecodefps.
- Updated VMAF.
- Updated vsSSIM.avsi.
- Updated bt2390_pq.avsi - optimized performance when peak_compute=false (~120% higher fps).
- Updated Butteraugli.
- Updated FastBlur.
- Updated AutoOverlay.
- Added [OverlayUtils.avsi](https://github.com/introspected/AutoOverlay/blob/master/OverlayUtils.avsi).
- Updated Average.
- Updated fft3dfilter.
- Updated fmtcavs.
- Updated InpaintDelogo.avsi.
- Updated masktools2.
- Updated MP_Pipeline.
- Added [Frfun7](https://github.com/pinterf/Frfun7/releases).
- Added [SmoothSkip](https://github.com/jojje/SmoothSkip/releases).
- Updated manyPlus.
- Updated EEDI2CUDA_AVS and file name changed to EEDI2CUDA.
- Added [overlayplus.avsi](http://avisynth.nl/index.php/OverlayPlus).
- Updated BM3D.
- Updated eedi3_resize16.avsi.
- Updated nnedi3_resize16.avsi.
- Updated ResizeX.avsi.
- Updated mClean.avsi and file name changed to mClean_.avsi.
- Added [VSmClean.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/Others/VSmClean.avsi).
- Updated AnimeIVTC.avsi.
- Updated FrameRateConverter.
- Updated Resize8.avsi.

## plugins_pack_r59

- Updated AutoResize_.avsi - added paramters mod2 and args.
- Updated ChubbyRain2_.avsi - replaced mt_convolution with Expr for >50% speed up.
- Updated rm_logo_.avsi - removed MinBlur function.
- Added [SSIM_downsample.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/SSIM_downsample.avsi).
- Updated AnimeMask.avsi - replaced GeneralConvolution with Expr for >3.5x speed up.
- Updated IQA_downsample.avsi - replaced GeneralConvolution with Expr for >3.5x speed up.
- Added [DPID](https://github.com/Asd-g/AviSynth-DPID/releases).
- Updated [L-SMASH-Works](https://github.com/HomeOfAviSynthPlusEvolution/L-SMASH-Works/releases).
- Updated RawSourcePlus.
- Updated bbmod_.avsi - fixed processing when the clip has only one plane.
- Updated balanceborders_.avsi - fixed processing when the clip has only one plane.
- Updated SmoothUV2.
- Updated avsresize - do not set matrix frame property when source matrix frame property is undef and color family is not changed; zimg@bf73dbe.
- Updated ffms2 -  ffms2@4567314; ffmpeg 4.4@1bc1757; zlib 1.2.11; dav1d 0.9.1; added FFAudioSource/FFMS2/FFmpegSource2 parameter "drc_scale" (float type, AC3 decoding).
- Updated [InpaintDelogo.avsi](https://github.com/Purfview/InpaintDelogo/blob/main/InpaintDelogo.avsi).
- Updated neo_FFT3D.
- Added [bbmodY.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/bbmodY.avsi) - apply bbmod from random column and row.
- Updated LinesLumaDiff.
- Added [dldet.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/dldet.avsi) - helper function for finding black/dirty lines on the borders.
- Added [BestAudioSource](https://github.com/vapoursynth/bestaudiosource) - bestaudiosource@a5b0669, ffmpeg 4.4@1bc1757.
- Added [fmtcavs](https://github.com/EleonoreMizo/fmtconv).
- Added [BM3D_VAggregate_AVS, BM3DCPU_AVS, BM3DCUDA_AVS](https://github.com/WolframRhodium/VapourSynth-BM3DCUDA/tree/avs+).
- Updated aWarpsharpMT.
- Added [ClipBlend](https://forum.doom9.org/showthread.php?t=168048).
- Added [ClipClop](https://forum.doom9.org/showthread.php?t=162266).
- Updated [FrameSel](https://forum.doom9.org/showthread.php?t=167971).
- Updated HDRTools.
- Updated nnedi3_resize16.avsi.
- Updated nnedi3.
- Updated SMDegrain_.avsi.
- Updated ResampleMT.
- Updated ResizeX.avsi.
- Updated [Resize8.avsi](https://forum.doom9.org/showthread.php?t=183057).
- Added [EEDI2CUDA_AVS](https://github.com/AmusementClub/VapourSynth-EEDI2CUDA/tree/AVS+).

## plugins_pack_r58

- Updated bt2390_pq.avsi - replaced FrameEvaluate with ScriptClip.
- Updated ComparisonGen.avsi - replaced FrameEvaluate with ScriptClip.
- Updated neo_f3kdb.
- Updated TimecodeFPS.
- Updated SmoothUV2.
- Updated rainbow_smooth2.avsi - changed back default hthresh to 220 (>= SmoothUV2 4.1.0).
- Updated ChromaShiftSP2_.avsi - changed UX/UY/VX/VY from chroma pixels to luma pixels.
- Updated ChubbyRain2_.avsi - made parameter interlaced dummy; removed restriction for only 420 video.
- Updated RawSourcePlus.
- Updated avsresize - registered as MT_MULTI_INSTANCE; read frame properties from every frame (previously only from the first frame); zimg@8d0b839.
- Updated FillBorders.
- Updated Ylevels_mt_.avsi - fixed Ylevels high bit depth gamma calculation.
- Updated FixChromaBleedingMod_.avsi - removed SmoothLevels dependency; fixed high bit-depth output.
- Updated VMAF.
- Updated mvtools2.
- Updated FrameRateConverter.
- Updated ExactDedup.
- Updated RgTools.
- Updated AutoOverlay.
- Updated dfttestMC_and_MC_things.avsi.
- Updated Khameleon_.avsi.
- Updated LSFmod.avsi.
- Updated UnsharpMask_avsi.avsi.
- Added [mClean.avsi](https://forum.doom9.org/showthread.php?p=1942879#post1942879).
- Updated QTGMC.avsi.
- Updated Zs_RF_Shared.avsi.
- Added [mRD_RestoreGrain.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/mRD_RestoreGrain.avsi). - https://forum.doom9.org/showthread.php?p=1942709#post1942709
- Updated SMDegrain.avsi and changed file name to SMDegrain_.avsi.
- Updated Xsharpen_avsi.avsi.
- Added [Small_Deflicker.avsi](https://forum.doom9.org/showthread.php?p=1812060#post1812060).
- Updated [xy-VSFilter](https://github.com/Masaiki/xy-VSFilter-pfmod/releases).
- Updated manyPlus.

## plugins_pack_r57

- Added [Bilateral](https://github.com/Asd-g/AviSynth-Bilateral/releases).
- Updated [hqdn3d](https://github.com/Asd-g/hqdn3d/releases).
- Updated D2VSource - fixed FFSAR_NUM, FFSAR_DEN, FFSAR.
- Added [vsDeGrainMedian](https://github.com/Asd-g/AviSynth-vsDeGrainMedian).
- Added [solarCurve](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/solarCurve.avsi).
- Updated bt2390_pq.avsi - added parameters colors, colors_weight, gain, gain_factor, removed dependency of GRunT.
- Updated Prewitt.avsi - make it work in any avs and some speed optimization for 8-bit input. (realfinder)
- Updated LSMASHSource.
- Updated SmoothUV2.
- Updated Rainbow_smooth_.avsi - added support for 10..16-bit clips; changed default hthresh from 220 to 125; SmoothUV2 >= 4.0.0 required; changed parameter mask - user mask could be used; function name and file name changed to rainbow_smooth2.
- Updated [Bifrost](https://github.com/Asd-g/AviSynth-bifrost/releases).
- Added [DotKill](https://github.com/Asd-g/AviSynth-DotKill/releases).
- Updated [ChubbyRain2.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/ChubbyRain2_.avsi) - file name chaged to ChubbyRain2_; replaced Cnr2 with vsCnr2; added support for 10..16-bit clips.
- Replaced Cnr2 with [vsCnr2](https://github.com/Asd-g/AviSynth-vsCnr2/releases).
- Updated GMSD.avsi - changed global scope of quality_map to local.
- Updated vsSSIM.avsi - changed global scope of ssim_map to local.
- Updated MDSI.avsi - change global scope of gcs to local.
- Updated ComparisonGen.avsi - removed dependency of GRunT.
- Updated LWLInfo.avsi - removed dependency of GRunT.
- Updated aWarpsharpMT.
- Updated HDRTools.
- Updated manyPlus.
- Updated NNEDI3.
- Updated ResampleMT.
- Added [TNLMeans](https://github.com/pinterf/TNLMeans/releases).
- Updated [waveform](https://forum.doom9.org/showthread.php?t=182866).
- Updated Abcxyz_MT2.avsi.
- Updated Advanced_Denoising.avsi.
- Updated AnimeIVTC.avsi.
- Updated ASTDR.avsi.
- Updated DeHalo_alpha_MT2.avsi.
- Updated DeHaloHmod.avsi.
- Updated edi_rpow2.avsi.
- Updated eedi3_resize16.avsi.
- Updated FineDehalo.avsi.
- Updated Masked_DHA.avsi.
- Updated nnchromaupsubsampling.avsi.
- Updated QTGMC.avsi.
- Updated Santiag.avsi.
- Added [UnsharpMask_avsi.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/UnsharpMask_avsi.avsi).
- Added [Xsharpen_avsi.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Xsharpen_avsi.avsi).
- Updated Zs_RF_Shared.avsi.
- Updated MasksPack.avsi.
- Added [GradePack_.avsi](https://github.com/Dogway/Avisynth-Scripts/blob/master/GradePack.v1.0.avsi) - disabled SmoothLevels.

## plugins_pack_r56

- Added [bt2390_pq.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/bt2390_pq.avsi).
- Updated Deblock - [SIMD optimized version](https://github.com/299792458m/Avisynth-Deblock).
- Updated FastBlur.
- Updated MyLWLInfo.avsi - added predefined colors and auto change text_size, bonus_text_size, bonus_text_pos when clip width < 1024. (pcroland)
- Updated ComparisonGen.avsi - fixed error when pixel_type="RGBP16"; added parameter check_type; if frame is not correct type, next frame will be checked until correct type is found; removed parameter frames_file; changed type of paratemer pframes_file from bool to string; added support for string values of paremeter frame_type.
- Added [AnimeMask.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AnimeMask.avsi).
- Added [AnimeMask2.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AnimeMask2.avsi).
- Added [vsTEdgeMask](https://github.com/Asd-g/AviSynth-vsTEdgeMask/releases).
- Updated ContraSharpen_mod_16_1.3_uMod_.avsi - replaced TEMmod with vsTEdgeMask.
- Updated timecodefps - allowed timestamp files created from mkvtoolnix.
- Updated AdaptiveGrain.avsi - make it work in any AVS, added keep_in_range.
- Updated JincResize.
- Updated nnedi3 (0.9.4.58).
- Updated aWarpsharpMT (2.1.4).
- Updated AddGrainC.
- Updated assrender.
- Added [Convolution3D](https://github.com/pinterf/Convolution3D/releases).
- Updated fft3dfilter.
- Updated AiUpscale.avsi and file name changed from AiUpscale_ to AiUpscale.
- Updated Shaders.
- Updated Advanced_Denoising.avsi.
- Updated AnimeIVTC.avsi.
- Updated ASTDR.avsi.
- Removed TEMmod.
- Updated ContraSharpen_mod.avsi.
- Updated dfttestMC.avsi and file name changed from dfttestMC to dfttestMC_and_MC_things.
- Updated mtmodes.avsi.
- Updated QTGMC.avsi.
- Updated scaled444tonative.avsi.
- Updated YAHR.avsi.
- Updated ZS_AVS_Shared_Functions.avsi and file name changed from ZS_AVS_Shared_Functions to Zs_RF_Shared.
- Added [sAddGrainCT.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/sAddGrainCT.avsi).
- Added [WarpDeRing.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/WarpDeRing.avsi).
- Added [UnsharpHQmod](https://github.com/299792458m/UnsharpHQmod).
- Updated AutoOverlay.
- Removed avsresize.readme.md.
- Removed fftw3.dll.
- Added [LinesLumaDiff](https://github.com/Asd-g/AviSynth-LinesLumaDiff/releases).

## plugins_pack_r55

- Updated D2VSource.
- Updated AutoResize_.avsi - "SD" mode: fixed target resolution when crop values are used.
- Updated BWDIF.
- Updated yadifmod2.
- Updated RawSourcePlus.
- Updated FillBorders.
- Updated Advanced_Denoising.avsi.
- Added [assrender](https://github.com/pinterf/assrender/releases).
- Updated autodeblock2.avsi.
- Updated MedianBlur2.
- Updated nnchromaupsubsampling.avsi.
- Updated QTGMC.avsi.
- Updated RemoveDirt.
- Added [RemoveGrainHD](https://github.com/pinterf/RemoveGrainHD/releases).
- Updated ResampleMT.
- Added [Rotate](http://avisynth.nl/index.php/Rotate).
- Updated scaled444tonative.avsi.
- Updated SMDegrain.avsi.
- Updated TComb.
- Updated TemporalDegrain2.avsi (also file name changed from TemporalDegrain-v2* to TemporalDegrain2).
- Updated TIVTC.
- Updated YAHR.avsi (also file name changed from YAHRmod to YAHR).
- Added [timecodefps](http://www.avisynth.nl/index.php/TimecodeFPS).
- Added [ExactDedup](http://avisynth.nl/index.php/ExactDedup).
- Added [DeDup](http://akuvian.org/src/avisynth/dedup/dedup.txt).

## plugins_pack_r54

- Updated JincResize.
- Updated HiAA_.avsi - moved helper functions outside the main function.
- Updated ComparisonGen.avsi -  added suport for up to 5 clips; added new parameter 'frame_type'; changed clip type to array; merged parameters 's_image_file' and 'e_image_file' to one parameter 'image_file' (type array); allowed RGB clips.
- Updated D2VSource.
- Updated bbmod_.avsi - added parameter "cloc".
- Updated balanceborders_.avsi - added parameter "cloc".
- Updated RT_Stats.
- Updated AutoOverlay.
- Added [warp](http://avisynth.nl/index.php/Warp).
- Updated AnimeIVTC.avsi.
- Updated neo_FFT3D.
- Updated RgTools.

## plugins_pack_r53

- Updated VideoTek.avsi.
- Updated UpscaleCheck.avsi - replaced myffinfo with MyLWLInfo.
- Updated spline36resizemod_.avsi - allowed 422 clips.
- Updated Delay.avsi - file name changed to Delay_.avsi; added parameter 'position'.
- Updated ffms2/ffmsindex - fixed MPEG-2 streams in mkv container; improved a bit dav1d speed; ffmpeg@66d89a8070; zlib 1.2.11; dav1d 0.8.0.
- Updated InpaintDelogo.avsi.
- Updated LSMASHSource - set variables FFSAR_NUM, FFSAR_DEN, FFSAR; ffmpeg@494f868e93; zlib 1.2.11; lsmash@18a9ed2; xxHash 0.8.0; dav1d 0.8.0.
- Updated LWLInfo.avsi - changed parameters from global to local; changed default values - only framenum and picturetype are true.
- Updated D2VSource - set variables FFSAR_NUM, FFSAR_DEN, FFSAR.
- Updated JincResize.
- Added [qCombed](https://forum.doom9.org/showthread.php?t=173754).
- Updated AnimeIVTC.avsi.
- Added [DDSharp.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/DDSharp.avsi).
- Updated fft3dfilter.
- Updated KNLMeansCL.
- Updated MP_Pipeline.
- Updated mvtools2.
- Updated TDeint.
- Updated TIVTC.
- Updated ZS_AVS_Shared_Functions.avsi.
- Added [VMAF](https://github.com/Asd-g/AviSynth-VMAF/releases).

## plugins_pack_r52

- Updated avsresize - use chromaloc frame property (when available) for the legacy resizers.
- Added [Butteraugli](https://github.com/Asd-g/AviSynthPlus-Butteraugli/releases).
- Updated AdaptiveGrain.avsi - assume the input and the output in full range for YUV float.
- Added MyLWLInfo.avsi - a simple version of LWLInfo displaying only frame number, picture type, cfr time.
- Added [FFTSpectrum](https://github.com/Asd-g/AviSynth-FFTSpectrum).
- Updated MCDegrain.avsi.
- Updated MCDegrainSharp.avsi.
- Updated GMSD.avsi - replaced mt_convolution with mt_edge for ~15% speed up.
- Updated Kirsch.avsi - added parameters: thY1, thY2, thC1, thC2. Removed default value for 'chroma'.
- Added [Prewitt.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/Prewitt.avsi).
- Updated AiUpscale.avsi and folder Shaders.
- Updated edi_rpow2.avsi.
- Added [HaloBuster.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/HaloBuster.avsi).
- Updated KNLMeansCL.
- Added [pSharpen.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/pSharpen.avsi).
- Updated scaled444tonative.avsi.
- Updated VideoTek.avsi.

## plugins_pack_r51

- Added vsSSIM.avsi - port of the VapourSynth SSIM.
- Added IQA_downsample.avsi - port of the VapourSynth _IQA_downsample.
- Added [vsTCanny](https://github.com/Asd-g/AviSynth-vsTCanny/releases).
- Removed TCannyMod.
- Updated ContraSharpen_mod_16_1.3_uMod_.avsi - replaced TCannyMod with vsTCanny.
- Updated Khameleon.avsi - replaced TCannyMod with vsTCanny, file name changed to Khameleon_.avsi.
- Added GMSD.avsi - port of the VapourSynth GMSD.
- Updated UpscaleCheck.avsi - changed parameter resolution to int for AutoResize func.
- Updated avsresize and avsresize.readme.md - fixed crashing when avs+ doesn't support frame properties; set _SarNum and _SarDen properties; read the input frame property _ChromaLocation if available; do not process clips with frame property _FiledBased > 0.
- Updated MCDegrain.avsi.
- Updated MCDegrainSharp.avsi.
- Updated AutoResize_.avsi - moved rounding function as standalone function (RoundHalfToEven.avsi); don't use mod2 when calculating the height of yv24/yv16.
- Added RoundHalfToEven.avsi.
- Added MDSI.avsi - port of the VapourSynth MDSI.
- Updated TIVTC.
- Updated AnimeIVTC.avsi.
- Updated dfttestMC.avsi.
- Updated LSFmod.avsi.
- Added [manyPlus](http://www.avisynth.nl/users/vcmohan/manyPlus/manyPlus.html).
- Updated sync_tools.avsi.
- Added [MasksPack](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/MasksPack.avsi).
- Updated ZS_AVS_Shared_Functions.avsi.
- Updated ContraSharpen_mod.avsi.

## plugins_pack_r50

- Updated ReduceFlicker.
- Updated spline36resizemod_.avsi - set float for the default values instead convert int to float.
- Added [ASharp](https://github.com/Asd-g/AviSynth-ASharp/releases).
- Updated balanceborders_.avsi - code clean up; fixed cBottom assert for chroma; arrays - if a single value is specified, it will be used for all planes, if two values are given then the second value will be used for the third plane as well.
- Updated bbmod_.avsi - code clean up; fixed cBottom assert for chroma; arrays - if a single value is specified, it will be used for all planes, if two values are given then the second value will be used for the third plane as well.
- Updated avsresize + avsresize.readme.md) - added parameter prefer_props; read and set _ChromaLocation, _ColorRange, _Matrix, _Transfer, _Primaries frame properties; added chromaloc_op parameters - bottom_left and bottom; added keyword same for destination matrix, transfer, primaries, range, chromaloc_op. For more info check the included avsresize.readme.md.
- Updated Tonemapper.avsi - improved scenes with < 100 nits. Changed algo to BT2446 Method A. Added clip8 parameter.
- Updated LWLInfo.avsi - added the other frame properties than picture type.
- Updated ComparisonGen.avsi - removed support for FFMS2 variables.
- Updated LSMASHSource - added the other frame properties than picture type, dav1d 0.7.1, ffmpeg@9b72cea446.
- Updated ffms2 + ffmsindex - added frame properties, dav1d 0.7.1, ffmpeg@9b72cea446.
- Updated D2VSource.
- Updated AutoResize_.avsi - round to the nearest even when the fraction is 0.5. Mode is changed to integer when mode "sd" is not used.
- Updated Advanced_Denoising.avsi.
- Updated AnimeIVTC.avsi.
- Added [autodeblock2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/autodeblock2.avsi).
- Updated avstp.
- Updated dfttestMC.avsi.
- Updated dither.
- Updated dither.avsi.
- Updated F3KDB_s.avsi.
- Added [LimitedSharpen2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LimitedSharpen2.avsi).
- Updated LUTDeCrawl.avsi.
- Updated nnedi3_resize16.avsi.
- Updated QTGMC.avsi.
- Updated ssubsampling_convert.avsi.
- Updated sync_tools.avsi.
- Updated TDeint.
- Updated ZS_AVS_Shared_Functions.avsi.

## plugins_pack_r49

- Added AdaptiveGrain.avsi - it uses adaptive mask based on frame and pixels brightness to apply grain. More info included in the script.
- Updated balanceborders_.avsi - fixed error when cTop/cBottom/cLeft/cRight=1.
- Update bbmod_.avsi - fixed error when cTop/cBottom/cLeft/cRight=1.
- Updated MCTD_.avsi.
- Updated FixBrightnessProtect3.avsi - allowed adj_val, prot_val, upper, prot_val2 to be non-array when row/column is array (FixCBr([1,2], 8)).
- Updated MTCombMask.
- Updated daa3MOD.avsi.
- Updated edi_rpow2.avsi.
- Updated eedi3_resize16.avsi.
- Updated MAA2.avsi.
- Updated nnchromaupsubsampling.avsi.
- Updated nnedi3_resize16.avsi.
- Updated ResizeX.avsi.
- Added [RemoveDirt](https://github.com/pinterf/RemoveDirt/releases).
- Added [RemoveDirtMC_SE.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/RemoveDirtMC_SE.avsi).

## plugins_pack_r48

- Updated balanceborders_.avsi - merged all calculations in one expression that leads to ~15-20% less memory usage, ~10% speedup with 16-bit clips.
- Updated FixBrightnessProtect3.avsi - cosmetic changes.
- Updated bbmod_.avsi - merged all calculations in one expression that leads to ~20-35% less memory usage, ~20% speedup with 16-bit clips, ~10% speedup with 10-bit clips; fixed output with clips in float bit depth.
- Updated AnimeIVTC.avsi.
- Updated masktools2.
- Updated QTGMC.avsi.
- Updated SMDegrain.avsi.
- Updated ZS_AVS_Shared_Functions.avsi.

## plugins_pack_r47

- Updated ffms2 and ffmsindex - ffmpeg@1c7e55dd50, dav1d 0.7.1.
- Updated LSMASHSource.
- Added LWLInfo.avsi.
- Removed LibavInfo.avsi.
- Removed LibavParse.bat.
- Updated ComparisonGen.avsi - added support for LSMASHSource picture type.
- Updated BWDIF.
- Updated vsTMM.
- Updated FillBorders.
- Updated MatchHistogram.
- Updated CAS.
- Updated vsTTempSmooth.
- Updated avsresize - zimg v3.0.1, added support for frame properties.
- Updated SangNom2.
- Added [vsLGhost](https://github.com/Asd-g/AviSynthPlus-vsLGhost/releases).
- Updated neo_f3kdb - changed back to r2 because the next versions have memory leak, added support for frame properties.
- Updated F3KDB_s.avsi - use neo_f3kdb for F3KDB_3_adg.
- Updated Advanced_Denoising.avsi.
- Updated AutoOverlay (AutoOverlayNative, AutoOverlay_netautoload, MathNet.Numerics, Simd).
- Updated daa3MOD.avsi.
- Updated dfttestMC.avsi.
- Updated QTGMC.avsi.
- Updated Vinverse_avsi.avsi.
- Updated ZS_AVS_Shared_Functions.avsi.
- Updated MCTD_.avsi - added support for 10..16-bit clips.

## plugins_pack_r46

- Updated AutoResize_.avsi - added Crop() for cropping values >(-)1.
- Updated BWDIF.
- Updated [FillBorders](https://github.com/Asd-g/AviSynth-FillBorders/releases).
- Updated FixBrightnessProtect3.avsi - added array support.
- Updated balanceborders_.avsi - added array support.
- Updated bbmod_.avsi - added array support.
- Updated F3KDB_s.avsi.
- Updated LSFmod.avsi.
- Updated masktools2.
- Updated STpresso.avsi.
- Updated UpscaleCheck.avsi - added new parameter show.
- Added [vsTMM](https://github.com/Asd-g/AviSynth-vsTMM/releases).
- Updated ZS_AVS_Shared_Functions.avsi.

## plugins_pack_r45

- Added [CAS](https://github.com/Asd-g/AviSynth-CAS/releases).
- Added [BWDIF](https://github.com/Asd-g/AviSynth-BWDIF/releases).
- Updated dfttest.
- Updated ComparisonGen.avsi.
- Updated MatchHistogram.
- Updated AutoResize.avsi and file name changed to AutoResize_.avsi.
- Updated DeSpot.
- Updated TIVTC.

## plugins_pack_r44

- Updated AutoResize.avsi.
- Updated LSMASHSource.
- Updated ResampleMT.
- Updated nnedi3.
- Replaced TBilateral with [vsTBilateral](https://github.com/Asd-g/AviSynth-vsTBilateral/releases).
- Updated TIVTC.
- Updated Advanced_Denoising.avsi.
- Updated AnimeIVTC.avsi.
- Updated DeHaloHmod.avsi.
- Updated dfttestMC.avsi.
- Updated scaled444tonative.avsi.
- Updated ZS_AVS_Shared_Functions.avsi.
- Added [ssubsampling_convert.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/ssubsampling_convert.avsi).
- Added [sync_tools.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/sync_tools.avsi).
- Added [Vinverse_avsi.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Vinverse_avsi.avsi).
- Added AviSynthPluginsDir.avsi.
- Added [AiUpscale.avsi + Shaders folder](https://github.com/Alexkral/AviSynthAiUpscale/releases) - changed the path for Shaders folder. File name changed to AiUpscale_.avsi.
- Updated DDComb_.avsi.

## plugins_pack_r43

- Updated yadifmod2.
- Updated TMM2.
- Updated JincResize.
- Added [vsDeblockPP7](https://github.com/Asd-g/AviSynth-vsDeblockPP7/releases).
- Updated Tonemapper.avsi.
- Update AnimeIVTC.avsi.
- Updated daa3MOD.avsi.
- Updated dfttestMC.avsi.
- Updated F3KDB_s.avsi.
- Updated LSFmod.avsi.
- Added [MCBob.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/MCBob.avsi).
- Updated ZS_AVS_Shared_Functions.avsi.

## plugins_pack_r42

- Updated bbmod_.avsi - removed an unnecessary resize call.
- Updated [EEDI2](https://github.com/Asd-g/AviSynth-EEDI2/releases).
- Removed MPEG2DecPlus_readme.md.
- Replaced MPEG2DecPlus with [D2VSource](https://github.com/Asd-g/MPEG2DecPlus/releases).
- Updated [AvsFilterNet](https://github.com/Asd-g/AvsFilterNet/releases).
- Updated [AutoOverlay](https://github.com/Asd-g/AutoOverlay/releases) (AutoOverlayNative, AutoOverlay_netautoload).
- Updated Shader.
- Updated DenoiseSelective.avsi.
- Updated GradFun2DBmod.avsi.
- Updated masktools2.
- Updated oSmoothLevels.avsi.
- Updated SMDegrain.avsi.
- Updated TIVTC.
- Added myselectrange.avsi.
- Updated neo_FFT3D.
- Updated neo-minideen.
- Updated neo_Vague_Denoiser.
- Updated dfttestMC.avsi.
- Updated F3KDB_s.avsi.
- Updated ZS_AVS_Shared_Functions.avsi.
- Added [vsMSmooth](https://github.com/Asd-g/AviSynth-vsMSmooth/releases).

## plugins_pack_r41

- Updated nnedi3.
- Updated MatchHistogram.
- Updated SmoothUV2.
- Updated DeSpot.
- Updated vsTTempSmooth.
- Added [JincResize](https://github.com/Asd-g/AviSynth-JincResize).
- Updated yadifmod2.
- Updated balanceborders_.avsi - replaced ResampleMT with z_ resizers.
- Updated bbmod_.avsi - replaced ResampleMT with z_ resizers.
- Updated MP_Pipeline.
- Updated mvtools2.
- Updated neo_FFT3D.
- Updated TDeint.
- Updated TIVTC.
- Updated Advanced_Denoising.avsi.
- Updated QTGMC.avsi.
- updated DFMDeRainbow.avsi.

## plugins_pack_r40

- Updated SangNom2.
- Updated yadifmod2.
- FrameRateConverter-x64 file name changed to FrameRateConverter_x64.
- Updated vsMSharpen.
- Updated ContraSharpen_mod_16_1.3_uMod.avsi - replaced MSharpen with vsMSharpen; file name changed to ContraSharpen_mod_16_1.3_uMod_.avsi.
- Updated HiAA.avsi - replaced MSharpen with vsMSharpen; file name changed to HiAA_.avsi.
- Added [DenoiseSelective.avsi](https://forum.doom9.org/showthread.php?p=1914252#post1914252).
- Updated MTCombMask.
- Replaced TTempSmooth with [vsTTempSmooth](https://github.com/Asd-g/AviSynth-vsTTempSmooth).
- Updated LSMASHSource.
- Updated neo_f3kdb.
- Updated neo_minideen.
- Updated ZS_AVS_Shared_Functions.avsi.
- Updated SMDegrain.avsi.
- Updated QTGMC.avsi.
- Updated LSFmod.avsi.
- Updated HQDeringmod.avsi.
- Updated GrainStabilizeMC.avsi.
- Updated DFMDeRainbow.avsi.
- Updated daa3MOD.avsi.
- Updated ASTDR.avsi.
- Updated Advanced_Denoising.avsi.
- Added [Santiag.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Santiag.avsi).
- Updated rm_logo.avsi - replaced TTempSmoothF with vsTTempSmooth; file name changed to rm_logo_.avsi.
- Update MCTD_.avsi - replaced TTempSmooth(F) with vsTTempSmooth.
- removed plugins_JPSDR.
- added [AutoYUY2](https://github.com/jpsdr/AutoYUY2/releases).
- added [aWarpsharpMT](https://github.com/jpsdr/aWarpSharpMT/releases).
- added [HDRTools](https://github.com/jpsdr/HDRTools/releases).
- added [NNEDI3](https://github.com/jpsdr/NNEDI3/releases) - 0.9.4.53. The latest two releases have bugs.
- added [ResampleMT](https://github.com/jpsdr/ResampleMT/releases).
- added [GradFun2DBmod.avsi](https://forum.doom9.org/showthread.php?t=144537).
- updated insertsign.avsi.

## plugins_pack_r39

- Updated CombMask - function names changed to CombMask2, MaskedMerge2, IsCombed2.
- Added [MTCombMask](https://github.com/Asd-g/MTCombMask/releases).
- Renamed (file name + function name) MSharpen to [vsMSharpen](https://github.com/Asd-g/AviSynth-vsMSharpen/releases).
- Updated [SangNom2](https://github.com/Asd-g/AviSynth-SangNom2/releases).
- Updated yadifmod2.
- Updated [ReduceFlicker](https://github.com/Asd-g/ReduceFlicker/releases).
- Updated AddGrainC.
- Updated DePan.
- Updated DePanEstimate.
- Updated mvtools2.
- Updated ZS_AVS_Shared_Functions.avsi.
- Updated LUTDeCrawl.avsi.
- Updated LUTDeRainbow.avsi.
- Updated HQDeringmod.avsi.
- Added [GrainStabilizeMC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/GrainStabilizeMC.avsi).
- Added [LRemoveDust.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LRemoveDust.avsi).
- Updated [DFMDeRainbow_.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/DFMDeRainbow.avsi) and file name changed to DFMDeRainbow.avsi.
- Added [unb_s.avs](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/unb_s.avsi).
- Added [insertsign.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/insertsign.avsi).

## plugins_pack_r38

- updated ComparisonGen.avsi - script info changed, fixed ScriptClip error message
- updated LibavInfo.avsi - added file_name parameter for the name of the new file, added default values for bat_path
- updated LibavParse.bat - cosmetic changes
- updated FixBrightnessProtect3.avsi - cosmetic changes
- updated [DeDot](https://github.com/Asd-g/AviSynth-DeDot/releases) - v8 interface
- updated [TMM2](https://github.com/Asd-g/TMM2/releases)
- updated [yadifmod2](https://github.com/Asd-g/yadifmod2/releases)
- updated [TCannyMod](https://github.com/Asd-g/TCannyMod/releases)
- updated [RawSourcePlus](https://github.com/Asd-g/RawSource_2.6x/releases)
- updated [MatchHistogram](https://github.com/Asd-g/AviSynth-MatchHistogram/releases) - v8 interface
- updated [MSharpen](https://github.com/Asd-g/AviSynth-MSharpen/releases) - v8 interface
- updated [SmoothUV](https://github.com/Asd-g/AviSynth-SmoothUV2/releases) - v8 interface, renamed only the file name to SmoothUV2 (the function is still SmoothUV)
- updated Advanced_Denoising.avsi
- updated LUTDeCrawl.avsi
- updated LUTDeRainbow.avsi
- updated nnchromaupsubsampling.avsi
- updated masktools2
- updated TDeint
- updated TIVTC
- updated [ASTDR.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/ASTDR.avsi)
- updated ZS_AVS_Shared_Functions.avsi

## plugins_pack_r37

- updated ReduceFlicker - v8 interface
- updated CombMask - v8 interface
- updated TMM2 - v8 interface
- updated RawSourcePlus - v8 interface
- updated DCTFilter - v8 interface
- updated FixBrightnessProtect3.avsi - removed full_range parameter, added float prot_val2 parameter for protecting pixels above and below threshold simultaneously
- updated MPEG2DecPlus - LumaYUV() v8 interface, removed Deblock function (duplicated with Deblock.dll)
- updated MPEG2DecPlus_readme.md - removed info about Deblock
- update TCannyMod - v8 interface
- updated yadifmod2 - removed AVX instruction set requirement
- updated TEMmod - v8 interface, ~7% speedup
- updated neo_FFT3D
- updated plugins_JPSDR
- updated Spresso.avsi
- updated STpresso.avsi
- updated SMDegrain.avsi
- updated [TDeint](https://github.com/pinterf/TIVTC)

## plugins_pack_r36

- updated Kirsch.avsi - added changes info;
- updated LibavInfo.avsi - added usage and changes info; now call LibavParse.bat when it's used for the first time; removed file_path parameter and added string "bat_path" (""), bool "first_time" (false);
- updated LibavParse.bat - added usage info; few internal changes;
- updated MCTD_.avsi - added changes info;
- updated MergeLMH_.avsi - added changes info;
- removed MinBlur.avsi - it's part of ZS_AVS_Shared_Functions_.avsi;
- updated Offset_video_.avsi - added changes info;
- updated Rainbow_smooth_.avsi - added changes info;
- updated spline36resizemod_.avsi - added changes info;
- updated SupT.avsi - added usage and changes info;
- renamed ImageGen.avsi to ComparisonGen.avsi - function name ImageGen is renamed to ComparisonGen; removed function ImageGen_frames; now creating images for source vs encode comparison could be done only with one script;
- removed ImageGen.bat;
- removed smaskmerge.avsi - not needed with the latest masktools2 versions;
- updated [GRunT](https://github.com/pinterf/GRunT);
- updated plugins_JPSDR;
- updated RgTools;
- updated LUTDeCrawl.avsi;
- updated LUTDeRainbow.avsi;
- updated DePan;
- updated [KNLMeansCL](https://github.com/pinterf/KNLMeansCL);
- updated DebilinearM_.avsi;
- updated Advanced_Denoising.avsi;
- added [oSmoothLevels.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/oSmoothLevels.avsi);
- updated YLevels_mt.avsi - fixed bug, high bit depth support, renamed to YLevels_mt_.avsi.

## plugins_pack_r35

- updated balanceborders_.avsi - added usage info; intermediate function name changed; replaced lutxyz -> 8-bit ~15% speedup, 10-bit ~75% speedup, 16-bit ~50% speedup; removed full_range parameter;
- updated bbmod_.avsi - added usage info, intermediate function name changed; replaced lutxyz for 8/10-bit -> 8-bit 2x less memory usage, 10-bit ~25% speedup; 16-bit ~10% speedup; removed full_range parameter;
- renamed ChromaShiftSP2.avsi to ChromaShiftSP2_.avsi;
- updated DDComb_.avsi - added changes info;
- updated Drawrect.avsi - updated changes info, renamed to Drawrect_.avsi;
- updated FFMS2.avsi - added changes info, renamed to FFMS2_.avsi;
- updated FixBrightnessProtect3.avsi - added usage and changes info;
- renamed FixChromaBleedingMod.avsi to FixChromaBleedingMod_.avsi;
- updated Fixer.avsi - added usage and changes info;
- renamed DFMDeRainbow.avsi to DFMDeRainbow_.avsi;
- updated debandmask.avsi - return Y clip; 8-bit ~3% speedup (mrad > 1); 10-bit ~10% speedup (mrad > 1); 16-bit ~10x speedup (mrad > 1); changed default values to 60,16,4,4,1; renamed to debandmask_.avsi;
- updated MiniDeen and renamed to neo-minideen r8;
- added [neo_FFT3D r6](https://github.com/HomeOfAviSynthPlusEvolution/neo_FFT3D);
- added [neo_Vague_Denoiser r1](https://github.com/HomeOfAviSynthPlusEvolution/neo_Vague_Denoiser);
- updated Advanced_Denoising.avsi;
- updated ZS_AVS_Shared_Functions.avsi;
- added [LUTDeCrawl.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LUTDeCrawl.avsi);
- added [UTDeRainbow.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LUTDeRainbow.avsi);
- updated masktools2 2.2.22;
- updated [HQDeringmod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/HQDeringmod.avsi);
- added [STpresso.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/STpresso.avsi);
- added [Spresso.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Spresso.avsi);
- added [EdgeCleaner.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/EdgeCleaner.avsi);
- added [FixBlendIVTC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FixBlendIVTC.avsi);
- updated Toon.avsi - added usage and changes info; added missing helper function; 8/10-bit ~10% speedup, 16-bit significant speedup; removed full_range parameter;
- updated avsresize / avsresize.readme - zimg@8815111; added pixel type RGBP8, RGBAP8, YUV444, YUV444P8, YUVA444P8, YUV422, YUV422P8, YUVA422P8, YUV420, YUV420P8 , YUVA420P8, YUV411, YUV411P8; added version.

## plugins_pack_r34

- updated AddBordersMod.avsi - added usage info;
- (only x86) added avstp;
- updated Tonemapper.avsi - fixed average brightness by using PQ->HLG with frame peak nits->709 color space; removed gamma_a(m) parameters;
- updated F3KDB_s.avsi;
- added [xy-VSFilter](https://forum.doom9.org/showthread.php?p=1895855#post1895855) (VSFilter.dll);
- updated neo_f3kdb;
- added [neo_TMedian](https://github.com/HomeOfAviSynthPlusEvolution/neo_TMedian);
- added [neo_DFTTest](https://github.com/HomeOfAviSynthPlusEvolution/neo_DFTTest);
- updated ZS_AVS_Shared_Functions.avsi;
- updated dfttestMC.avsi.

## plugins_pack_r33

- updated MCTD_.avsi - replaced SangNom with SangNom2;
- added [AutoOverlay](https://github.com/introspected/AutoOverlay/releases) (AutoOverlay_netautoload.dll, AutoOverlayNative.dll, AvsFilterNet.dll);
- updated Advanced_Denoising.avsi;
- updated ContraSharpen_mod.avsi;
- updated [Deblock_QED_.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Deblock_QED_MT2.avsi) and renamed to Deblock_QED_MT2.avsi;
- updated [GrainFactory3.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/GrainFactory3.avsi);
- added [GrainFactory3mod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/GrainFactory3mod.avsi);
- updated ZS_AVS_Shared_Functions.avsi;
- added [nnchromaupsubsampling.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/nnchromaupsubsampling.avsi);
- added [scaled444tonative.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/scaled444tonative.avsi);
- updated nnedi3_resize16.avsi;
- updated avsresize - zimg@5896a26, changed z_BicubicResize default a, b to 0, 0.5;
- added [IResize.avsi](https://raw.githubusercontent.com/realfinder/AVS-Stuff/Community/avs%202.5%20and%20up/IResize.avsi);
- updated DebilinearM_.avsi;
- added ImageGen.avsi and ImageGen.bat for creating source and encode images - for usage read ImageGen.avsi/ImageGen.bat. [avsr for 32-bit avs+, avsr64 for 64-bit avs+](https://forum.doom9.org/showthread.php?t=173259) required. It should be placed in the same place as ImageGen.bat or in PATH. Also it's recommended to update DevIL.dll:
    - Download [DevIL 1.8.0 SDK for Windows](http://openil.sourceforge.net/download.php).
    - For avs+ 32-bit from lib\x86\Release\ copy DevIL.dll to C:\Windows\SysWOW64.
    - For avs+ 64-bit from lib\x64\Release\ copy DevIL.dll to C:\Windows\System32.
- updated [AddGrain 1.8.1](https://github.com/pinterf/AddGrainC) and renamed to AddGrainC;
- updated masktools2 2.2.21;
- added [FrameRateConverter](https://github.com/mysteryx93/FrameRateConverter) (FrameRateConverter(x64).dll, FrameRateConverter.avsi);
- added [FineSharp.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FineSharp.avsi);
- added [Hysteria.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Hysteria.avsi).

## plugins_pack_r32

- updated ZS_AVS_Shared_Functions.avsi;
- update daa3MOD.avsi;
- added [RoboCrop](http://avisynth.nl/index.php/RoboCrop);
- updated FineDehalo.avsi;
- updated [MedianBlur2](https://github.com/pinterf/MedianBlur2);
- updated [MP_Pipeline](https://github.com/pinterf/MP_Pipeline) (MP_Pipeline.dll, MP_Pipeline.dll.slave.exe, MP_Pipeline.dll.win*, MP_Pipeline.dll.win*.slave.exe).

## plugins_pack_r31

- updated LSMASHSource;
- updated Advanced_Denoising.avsi;
- updated ZS_AVS_Shared_Functions_.avsi and renamed to ZS_AVS_Shared_Functions.avsi;
- updated [dfttest](https://github.com/pinterf/dfttest/) - v1.9.6;
- updated [abcxyz.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Abcxyz_MT2.avsi) and renamed to Abcxyz_MT2.avsi;
- updated BlindDeHalo3_mt2.avsi;
- updated [dfttestMC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/dfttestMC_and_MC_things.avsi);
- updated nnedi3_resize16_.avsi and renamed to nnedi3_resize16.avsi;
- updated ContraSharpen_mod.avsi;
- updated ffms2/ffmsindex - ffmpeg n3@72be5d4661, dav1d 0.6.0;
- updated daa3MOD.avsi;
- updated bbmod_.avsi - another change for chroma processing; added Y/YUV422/YUV444 processing; ~20% speed increase for >8-bit;
- updated balanceborders_.avsi - added Y/YUV422/YUV444 processing;
- updated Masked_DHA.avsi.

## plugins_pack_r30

- updated DDComb.avsi - replaced frfun7 denoiser with TBilateral denoiser when strong=false and clip is proggressive; added option "sf" (string) to use external denoiser instead TBilateral; renamed to DDComb_.avsi;
- added [MCTD.avsi (MCTemporalDenoiseMod)](https://forum.doom9.org/showpost.php?p=1559222&postcount=625) - changed int "limit" to float "limit and int "limitC" to float "limitC"; changed GradFun2db to GradFun3 in prefiltering and in post-denoising (for the internal filter); renamed to MCTD_.avsi;
- added [GradFun2db](http://avisynth.nl/index.php/GradFun2db);
- updated YAHRmod.avsi;
- added [AutoLevels V0.12Beta3](https://forum.doom9.org/showthread.php?p=1878929);
- added Tonemapper.avsi - usage Tonemapper(clip, "float "gamma_a", float "gamma_m", bool "show"):
    - gamma_a has effect only when one of the planes in RGB linear has average > 2.5 * (the sum of the other planes). It applies gamma 0.42 (default) to the pixels with luma > 192 to prevent clipping;
    - gamma_m has effect only when luma plane has max values (235) after tonemapping. It applies gamma 0.65 (default) only to the pixels with luma > 230 to prevent clipping;
    - show (default false) - it's showing the nits value used for the HDR->SDR conversion and linear AverageR/G/B.

## plugins_pack_r29

- updated FixBrightnessProtect3 - processing also Y clips;
- added [MergeLMH.avsi](https://forum.doom9.org/showthread.php?s=3aa23a52382ef3b41c24eb0ad558ab91&p=1691736#post1691736) - added high bit depth support, renamed to MergeLMH_.avsi;
- updated balanceborders.avsi - additional options bool y (true), bool u (true), bool v (true) for processing different planes; renamed to balanceborders_.avsi;
- updated bbmod.avsi - additional options bool y (true), bool u (true), bool v (true) for processing different planes (processing only luma in 16-bit gives ~45% speed boost, only chroma in 16-bit ~100% speed boost); fixed chroma filtering; renamed to bbmod_.avsi;
- updated spline36resizemod.avsi - added high bit depth support, renamed to spline36resizemod_.avsi;
- updated Advanced_Denoising.avsi;
- updated AnimeIVTC.avsi;
- added [AnimeIVTC_helper.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/AnimeIVTC_helper.avsi);
- added [BlindDeHalo3_mt2.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/BlindDeHalo3_mt2.avsi);
- updated ContraSharpen_mod_3.4_uMod.avsi and renamed to ContraSharpen_mod.avsi;
- updated DDComb_.avsi and renamed to the original name without "_" suffix;
- updated DeHaloHmod.avsi;
- updated DeHalo_alpha_MT2.avsi;
- renamed Deblock_QED.avsi to Deblock_QED_.avsi;
- added [F3KDB_s.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/F3KDB_s.avsi);
- updated FastLineDarken.avsi;
- updated FineDehalo.avsi;
- updated LSFmod.avsi;
- added [Masked_DHA.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/Masked_DHA.avsi);
- updated QTGMC.avsi;
- updated ResizeX.avsi and renamed to the original name without "_" suffix;
- updated SMDegrain.avsi;
- updated SeeSaw.avsi;
- updated Srestore.avsi;
- added [ZS_AVS_Shared_Functions.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/ZS%20AVS%20Shared%20Functions.avsi) - fixed DDComb helper function and added several missing helper functions; renamed to ZS_AVS_Shared_Functions_.avsi;
- updated dither.avsi;
- added [eedi3_resize16.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.6%20and%20up/eedi3_resize16.avsi);
- updated nnedi3_resize16.avsi - fixed few bugs after updating and renamed to nnedi3_resize16_.avsi;
- added [smaskmerge.avsi](https://github.com/realfinder/AVS-Stuff/blob/master/avs%202.5%20and%20up/smaskmerge.avsi);
- updated DGHDRtoSDR;
- updated avsresize - zimg 2.9.3 (@4b02873); changed default pixel range for non-float->float / float->non-float conversion; finished alpha plane implementation;
- updated MiniDeen;
- updated mcDAA3.avsi and renamed to daa3MOD.avsi;
- updated MAA2.avsi;
- updated edi_rpow2.avsi.

## plugins_pack_r28

- updated Offset_video.avsi:
    - made parameters x, y optional;
    - use crop+addborders(mod) because is a bit faster then manipulating and stacking the frame;
    - file renamed to Offset_video_.avsi;
    - added new optional options: bool "AddBorders" (true), bool "AddBordersMod" (false), float "lsat" (0.88), float "tsat" (0.20), float "rsat" (0.4 when x > 2, otherwise 0.28), float "bsat" (0.20).
- (only 32-bit) - updated avsresize - plugins_pack_r27 has 64-bit version in plugins+.[/spoiler]

## plugins_pack_r27

- updated MCDegrainSharp.avsi;
- updated MCDegrain.avsi;
- added [vscube](https://forum.doom9.org/showthread.php?p=1863846#post1863846);
- updated avsresize - changed alpha plane type;
- updated MSharpen - fixed crash with RGB clips;
- added [VideoTek.avsi](https://forum.doom9.org/showthread.php?p=1832763#post1832763);
- updated ffms2, ffmsindex - ffmpeg@12bf46e; dav1d 0.5.2.

## plugins_pack_r26

- updated LSMASHSource;
- added SmoothUV - ported from [the vs version](https://github.com/dubhater/vapoursynth-smoothuv); usage: SmoothUV(clip, int radius (default 3), int threshold (default 270), bool interlaced (default auto detect based on Is.FiledBased())); for more info check the link of the vs version;
- updated DeDot - fixed crash when NewVideoFrame has different frame pitch than the source clip (SeparateFields());
- updated Kirsch.avsi - added optional additional parameters: u, v, chroma. The same usage as for masktools2;
- added [Rainbow_smooth_.avsi](https://forum.doom9.org/showthread.php?p=1025503#post1025503) - added optional parameter "mask" ("original", "hprewitt", "prewitt", "kirsch");
- updated masktools2;
- updated avsresize, avsresize.readme - zimg pre-release 2.9.254; added spline64; added support for alpha plane processing; added additional cpu types for avx512; added chromaloc_op option for z_...Resize functions; for more info check avsresize.readme.md;
- updated SMDegrain_.avsi, Srestore_.avsi, TemporalDegrain-v2.2.1_.avsi, VHSHaloremover_.avsi, abcxyz_.avsi - reverted back use_expr value, renamed to the original names without "_" suffix;
- updated Advanced_Denoising_.avsi, DeHaloHmod_.avsi, DeHalo_alpha_MT2_.avsi, FastLineDarken_.avsi, FineDehalo_.avsi, LSFmod_.avsi, QTGMC_.avsi, SeeSaw_.avsi - reverted back clamp_float and use_expr value, renamed to the original names without "_" suffix;
- updated DebilinearM_.avsi, ResizeX_.avsi - reverted back use_expr value;
- added [Offset_video.avsi](https://forum.doom9.org/showthread.php?t=178671).

## plugins_pack_r25

- updated LSMASHSource;
- updated DebilinearM.avsi - changed DeResizeMT to true; changed use_expr to process high bit depth; renamed DebilinearM.avsi to DebilinearM_.avsi;
- added [CallCmd](https://forum.doom9.org/showthread.php?t=166063);
- added [RawSourcePlus](https://github.com/chikuzen/RawSource_2.6x/tree/plus_mt) - changed YUV420P10/12/14 mapping to the corresponding avs+ color formats (previously used only YUV420P16);
- updated masktools2;
- updated waveform - clang build with better speed;
- updated FixBrightnessProtect3.avsi - added alias FixRowBrightnessProtect2 / FixColumnBrightnessProtect2 for backward compatibility;
- updated Advanced_Denoising.avsi, DeHaloHmod.avsi, DeHalo_alpha_MT2.avsi, FastLineDarken.avsi, FineDehalo.avsi, LSFmod.avsi, QTGMC.avsi - changed clamp_float type bool to int; changed use_expr to process high bit depth; renamed by adding suffix "_";
- updated SeeSaw.avsi - changed clamp_float type bool to int; renamed to SeeSaw.avsi to SeeSaw_.avsi;
- updated abcxyz.avsi, SMDegrain.avsi, Srestore.avsi, TemporalDegrain-v2.2.1.avsi, VHSHaloremover.avsi, ResizeX.avsi - changed use_expr to process high bit depth; renamed by adding suffix "_";
- updated DDComb.avsi - fixed missing arg; renamed to DDComb_.avsi.

## plugins_pack_r24

- updated AddBordersMod.avsi - AddBorders is used for the left side when lsat=1;
- added [Drawrect.avsi](https://forum.doom9.org/showthread.php?t=162754) - added high bit depth support and an optional parameter bool "full_range"* (default false);
- updated FastBlur;
- updated FixBrightnessProtect3.avsi - added an optional parameter bool "full_range"*;
- updated LSMASHSource;
- removed colors_rgb.avsi and colors_rgb.txt - they are coming with Avisynth+ releases;
- updated masktools2 - 2.2.20.

## plugins_pack_r23

- added AddBordersMod.avsi
    - Usage: - AddBordersMod(clip c, int "left", int "top", int "right", int "bottom", float "lsat", float "tsat", float "rsat", float "bsat", int "color");
    - all parameters are optional;
    - lsat (left side, default 0.88), tsat (top, default 0.20), rsat (right side, default 0.2 for 2px borders / 0.4 for >2px borders), bsat (bottom, default 0.20) - those parameters control the saturation;
    - the default saturation values are set for extreme cases (solid colors) so for more real case scenario lsat=1, tsat=0.4/0.5, rsat=0.4/0.5 (for both cases: bordes > 2px and borders = 2px), bsat=0.4/0.5 should be a good starting point;
    - color parameter (default black);
    - only 4:2:0 color format is supported.
    - Due to MPEG-2 subsampling dirty lines / chroma bleeding could appear when AddBorders is used (more visible when the area is very saturared). This script should mitigate those effects.
- updated FastBlur;
- updated Fixer.avsi
    - New parameters added: int "uleft", int "utop", int "uright", int "ubottom", int "uradius", int "vleft", int "vtop", int "vright", int "vbottom", int "vradius", bool "u" , bool "v". Default values are the same as cleft, ctop, cright, cbottom, cradius. "u" and "v" give the opportunity to process only u / v plane or both planes with different settings.
    - For processing both chroma planes with same settings "chroma" option is enough.
    - "chroma" and "u" / "v" cannot be true at the same time, when "chroma" is true "u" and "v" are false and vice versa.
    - "chroma", "u", "v" are false by default when luma is processed too but when only c(u)(v)left / c(u)(v)top / c(u)(v)/right / c(u)(v)bottom option(s) is/are used "chroma" / "u" / "v" are true and luma is false - for example:
        - Fixer(ctop=2) = Fixer(ctop=2, luma=false, chroma=true);
        - Fixer(uleft=2, vbottom=4) = Fixer(uleft=2, vbottom=4, luma=false, u=true, v=true).
- added [UpscaleCheck.avsi](https://gist.github.com/pcroland/c1f1e46cd3e36021927eb033e5161298);
- updated plugins_JPSDR - HDRTools 0.6.0.

## plugins_pack_r22

- updated AddGrain - workaround for assert when processing small dimensions clips;
- added [ColourWarp](https://forum.doom9.org/showthread.php?t=177120);
- updated Corners.avsi;
- updated DGDecode - dll filename changed to MPEG2DecPlus(64), build from [@7070644](https://github.com/chikuzen/MPEG2DecPlus); added [MPEG2DecPlus_readme.md](https://forum.doom9.org/showthread.php?p=1891656#post1891656);
- updated LSMASHSource;
- added MatchHistogram - [port from the vs updated version](https://github.com/dubhater/vapoursynth-matchhistogram), usage - MatchHistogram(clip, clip1, clip2, bool raw (default false), bool show (default false), bool debug (default false), int smoothing_window (default 8), bool y (default true), bool u (default false), bool v (default false));
- changed dll name Shader-x64 to Shader_x64 to avoid AvsPmod warning.

## plugins_pack_r21

- added [Deblock_QED.avsi](https://forum.doom9.org/showthread.php?p=1767112#post1767112) - added high bit depth support (8...32-bit); added bool full_range* parameter (default=false) for processing high bit depth - full_range=false usually for limited range YUV;
- updated GrainFactory3.avsi - allowed 32-bit input; added bool full_range* parameter (default=false) for processing high bit depth - full_range=false usually for limited range YUV;
- updated [LSMASHSource](https://github.com/HolyWu/L-SMASH-Works/releases);
- updated LibavParse.bat - changed processing for the new index format;
- added [AviSynthShader](https://github.com/mysteryx93/AviSynthShader) (Shader.avsi, Shader(-x64).dll, D3DX9_43.dll);
- added [TemporalDegrain2.avsi](http://forum.doom9.net/showthread.php?p=1854679);
- updated balanceborders.avsi - fixed default thresh value; allowed 32-bit input; added bool full_range* parameter (default=false) for processing high bit depth - full_range=false usually for limited range YUV;
- updated Toon.avsi, bbmod.avsi - allowed 32-bit input; added bool full_range* parameter (default=false) for processing high bit depth - full_range=false usually for limited range YUV;
- updated ffms2, ffmsindex.exe - ffmpeg n4.3_9cd56bb; dav1d 0.5.1; default threads=1 for AV1;

full_range* - it works only for high bit depth; it determines how the scaling to the input bit depth is done ([for more info](http://avisynth.nl/index.php/MaskTools2#Expression_syntax_supporting_bit_depth_independent_expressions))

## plugins_pack_r20

- added [DCTFilter](https://github.com/chikuzen/DCTFilter);
- updated MSharpen - [port of vs MSharpen](https://github.com/dubhater/vapoursynth-msmoosh); default values - MSharpen(threshold=6.0, strenght=39.0, mask=false, luma=true, chroma=false, highq=true), highq - is a dummy parameter for backward compatibility;
- updated Toon.avsi - removed the helper functions; fixed the 16-bit processing;
- updated YLevels_mt.avsi - added restrictions "input_low and input_high must be different" and "output_low and output_high must be different" to prevent division by zero; fixed show_function type int -> bool and the processing when true;
- balanceborders.avsi / bbmod.avsi - changed the thresh type int -> float, thresh is always in the range [0,128] (before it was in the range [0,bit depth half range]).

## plugins_pack_r19:

- added [Advanced_Denoising.avsi](https://pastebin.com/3Fnru0B5);
- added [AnimeIVTC.avsi](https://forum.doom9.org/showthread.php?t=170364);
- added Daa3.avsi;
- added Delay.avsi;
- added EEDI2;
- updated FillBorders - added FillMargins function for backward compatibility; it's just alias for FillBorders(mode=0);
- added [ReduceFlicker](https://github.com/chikuzen/ReduceFlicker) - built from the latest commit;
- added Toon.avsi - port of vs havsfunc.Toon;
- added vinverse.

## plugins_pack_r18

- removed ConvertStacked, DirectShowSource, ImageSeq, Shibatch, TimeStretch, VDubFilter - these plugins are coming with Avisynth+ releases.\
    **You must redownload the plugins from Avisynth+ r2915 (ConvertStacked.dll, DirectShowSource.dll, ImageSeq.dll, Shibatch.dll, TimeStretch.dll, VDubFilter.dll) if you're using Avisynth+ r2915 from this thread and you have plugins_pack_r14 or later**;
- updated FFMS2.avsi - added new options:
    - frametypenos - doesn't display the frametype string (Intra, Bi-dir predicted and so on), default=false. When frametype=false frametypenos=true and vice versa. To disable completely frametype - frametype=disable and frametypenos=disable;
    - font;
    - size;
    - textcolor - [the values are HEX](http://avisynth.nl/index.php/Preset_colors);
    - halocolor - again HEX values;
    - align *;
    - spc *;
    - lsp *;
    - fontwidth *;
    - fontangle *;
    - x *;
    - y *.
    - \* for more information check the equivalent options [HERE](http://avisynth.nl/index.php/Subtitle);
- updated LibavInfo.avsi - added new options (same as FFMS2.avsi except frametypenos);
- removed FillBorder.avsi - use FillBorders;
- removed FillMargins - use FillBorders;
- updated Fixer.avsi - all options are optional (Fixer(2), Fixer(right=1) and so on). Added restriction - clip width / height should be > left+2, rgith+2 / top+2, bottom+2 or chroma width / height should be > cleft+1, crgith+1 / ctop+1, cbottom+1 when only chroma is processed;
- updated balanceborders.avsi and bbmod.avsi - all options are optional (bbmod(2), balanceborders(cleft=1) and so on). Added restriction - clip width / height should be > cLeft\*4, cRgith\*4 / cTop\*4, cBottom\*4.

## plugins_pack_r17

- added DGDecode;
- updated FixC(R)Br - added "upper" option (default true), when true pixels with higher luma than prot_val are protected, false - pixels with lower luma than prot_val are protected; now prot_val (float) = luma threshold; prot_val is in range [0,255] for all bit depth;
- updated LSMASHSource - AV1 support (dav1d);
- updated ffms2 + ffmsindex - aom replaced with dav1d for AV1 decoding, when decoding AV1 it's recommended to set threads=1 for accurate seeking (still ~50% faster than aom decoding); now max threads are changed to min(0.75*cpu threads, 16) (previous - min(cpu threads, 16)) without performance loss but VP9 should be accurate seeking by default.

## plugins_pack_r16

- updated DFMDeRainbow.avsi;
- updated LSFmod.avsi;
- updated LSMASHSource;
- updated ffms2 - some HEVC mkv files aren't seeking accurate after the previous update, now they are.

## plugins_pack_r15

- updated Info2;
- updated InpaintDelogo.avsi;
- updated LSMASHSource - updated ffmpeg, aom; fixed several seeking issues;
- updated LibavParse.bat - parsing only the video stream when LWLibavVideoSource is used (for LSMASHSource versions before ~17.08.2019); the location and the name of the index file have to be entered without quotes;
- updated MiniDeen;
- added [RemapFrames](http://avisynth.nl/index.php/RemapFrames);
- updated mcDAA3.avsi;
- updated neo_f3kdb;
- updated nnedi3_resize16.avsi;
- updated plugins_JPSDR.

## plugins_pack_r14

- updated AVSInpaint, AddGrain, Hqdn3dY, MiniDeen, avsresize, checkmate, masktools2 - removed AVX instruction set requirement
- added [DFMDeRainbow.avsi](https://forum.doom9.org/showthread.php?t=171715) - replaced Deen with MiniDeen
- updated MSharpen - registered as MT_NICE_FILTER
- updated DeHaloHmod.avsi
- updated DeHalo_alpha_MT2
- updated DeSpot - ~10% faster build
- updated Fixer.avsi - fixed default chroma radius when only chroma processed
- added [HQDeringmod.avsi](http://avisynth.nl/index.php/HQDering_mod)
- added [InpaintDelogo.avsi](https://forum.doom9.org/showthread.php?t=176860)
- added [Khameleon.avsi](https://forum.doom9.org/showthread.php?t=174770)
- added Kirsch.avsi - Kirsch algorithm for edge mask
- updated LSFmod.avsi
- updated LSMASHSource - updated ffmpeg; added cache file option; fix some VC-1 issues
- updated RgTools
- updated Srestore.avsi
- added TUnsharp
- updated debandmask.avsi - added default values that give ~same result as mt_edge("prewitt") but ~2x slower; however debandmask(60,16,4,4,1) with predenoised source gives better result than mt_edge("prewitt")
- updated ffms2, ffmsindex - updated ffmpeg; workaround some .mp4 seeking issues
- restored original flash3kyuu_deband (f3kdb with input/output mode) - ~8% faster clang build
- added [neo_f3kdb](https://github.com/HomeOfAviSynthPlusEvolution/neo_f3kdb) - f3kdb with removed doubleweave/stacked format; removed input/output mode option; native high bit depth support; added sample_mode 3/4

## plugins_pack_r13

- added [DDComb.avsi](https://pastebin.com/V15XkSAK)
- added Hqdn3dY
- added [SeeSaw.avsi](https://pastebin.com/hqyLDArX)
- added [VHSHaloremover.avsi](https://pastebin.com/u3B4WFT2)
- added [abcxyz.avsi](https://pastebin.com/UL6ycpbp)
- updated avsresize - zimg 2.9.2
- updated dfttestMC
- updated masktools2 - 2.2.19

## plugins_pack_r12a

- changed LSFmod.avsi (only x86) encoding from UTF-8 BOM (avs+ doesn't like it) to UTF-8

## plugins_pack_r12

- updated AddGrain - [port of vs version](https://github.com/HomeOfVapourSynthEvolution/VapourSynth-AddGrain); high bit depth support (8...32-bit); ~10% faster than 1.7.1 version
- updated DeHaloHmod.avsi
- added [DeSpot](https://forum.doom9.org/showthread.php?t=173259)
- updated GrainFactory3.avsi - high bit depth support (8...16-bit)
- updated ResizeX.avsi
- updated edi_rpow2.avsi

## plugins_pack_r11a

- changed FastLineDarken.avsi and SMDegrain.avsi encoding from UTF-8 BOM (avs+ doesn't like it) to UTF-8

## plugins_pack_r11

- updated ASTDR.avsi
- updated DeHalo_alpha.avsi
- updated DebilinearM.avsi
- updated FastLineDarken.avsi
- updated FineDehalo.avsi
- added GradFun3mod.avsi
- added GrainFactory3.avsi
- updated LSFmod.avsi
- updated MAA2.avsi
- added [MiniDeen](https://github.com/HomeOfAviSynthPlusEvolution/MiniDeen/releases) - ~20% faster clang build than the one on github; removed y/u/v=1 (not process the plane); working with both i420 and YV12 color spaces (YUV/YVU)
- updated QTGMC.avsi
- updated ResizeX.avsi
- updated SMDegrain.avsi
- added [checkmate](https://github.com/tp7/checkmate) - faster ~5% clang build than the one on github

## plugins.pack.r10
- added [DebilinearM.avsi](https://forum.doom9.org/showpost.php?p=1879523&postcount=93) - stacked and native high bit depth formats supported
- updated LSMASHSource - removed stacked/double width format; native high bit depth supported; AV1 support added
- updated ResizeX.avsi
- updated ffms2/ffmsindex - 5-10% faster build; AV1 support added; fixed VP9 decoding

## plugins.pack.r9

- updated [AVSInpaint](https://github.com/pinterf/AvsInpaint) - added clang build up to ~50% faster than [msvc build](https://github.com/pinterf/AvsInpaint/releases)
- added [FastBlur](https://forum.doom9.org/showthread.php?t=176564)
- added [Info2](https://forum.doom9.org/showthread.php?t=176563)
- updated [flash3kyuu_deband](https://github.com/msg7086/flash3kyuu_deband) - removed stacked format; Avisynth+ native high bit depth support; added intel compiler build ~10% faster than [msvc build](https://forum.doom9.org/showpost.php?p=1878157&postcount=18)

## plugins.pack.r8

- updated DGHDRtoSDR
- added DGPQtoHLG
- updated InpaintFunc.avsi
- updated avsresize (the latest zimg with Spline36 fixed)
- updated ffms2/ffmsindex (fixed VC-1 1 frame off)

## plugins.pack.r7

- updated avsresize - used previous zimg version since the latest one has a bug (spline36 wrong output)
- added [Duplicity2/DropDeadGorgeous](https://forum.doom9.org/showthread.php?s=d7bbd2678054fea84f077c73caac325c&t=175357)

## plugins.pack.r6
- added [ASTDR.avsi](https://forum.doom9.org/showpost.php?p=1665492&postcount=27)
- added DeCross - [vs v2 port](https://github.com/dubhater/vapoursynth-decross)
- added [Deflicker](https://github.com/pinterf/Deflicker)
- added [Estimate_Nnedi3_Rpow2.avsi](https://forum.doom9.org/showpost.php?p=1875042&postcount=1)
- updated Fixer.avsi - added optional parameter "luma" (default=true). When luma=false and chroma=true only chroma planes are processed
- added updated [plugins_JPSDR](https://github.com/jpsdr/plugins_JPSDR) and removed ResampleMT, aWarpsharpMT, nnedi3, HDRTools
- updated LSMASHSource (latest lsmash and ffmpeg)
- added [LoadDLL](https://forum.doom9.org/showpost.php?p=1759271&postcount=1)
- added [MCDegrain](https://forum.doom9.org/showpost.php?p=1874531&postcount=21)
- added [MCDegrainSharp](https://forum.doom9.org/showpost.php?p=1508638&postcount=12)
- updated SMDegrain
- added [aWarpSharp4xx](https://pastebin.com/qLkBTbiF)
- added autoLoadDLLs, moved libfftw3f-3 and fft3w from external folder to plugins folder - now they are loaded auto from plugin folder; no need copying them to external folder
- added [hqdn3d](http://avisynth.nl/index.php/Hqdn3d) (new faster builds)
- added [mcDAA3.avsi](https://forum.doom9.org/showthread.php?p=1639679#post1639679)
- removed msvcrxxx - it's better using [this AIO pack](https://github.com/abbodi1406/vcredist/releases)
- updated avsresize (latest zimg)
- updated ffms2 (latest ffmpeg)
- added [dfttestMC](https://pastebin.com/zax90CVq)
- added [edi_rpow2](https://pastebin.com/mkYiBwUq)

## plugins.pack.r5

- updated DePan and DePanEstimate
- added [FFMS_MakeMultiPartScripts.avsi and FFMS_MakeMultiPartScripts_Client.avs](https://forum.doom9.org/showpost.php?p=1873783&postcount=2)
- updated FluxSmooth
- added LibavParse.bat and LibavInfo.avsi:
    - A function similar to FFInfo for LWLibavVideoSource.
    - LibavParse.bat creates txt file from the index file (*.lwi). This file contains info about the frame type and the frame number. When the batch file is started, you have to:
        1. Enter the location of index file: - the location of .lwi file.
        2. Enter the name of index file: - the name of .lwi file.
        3. Enter the location for the parsed file: - the location of the new created file. This line is optional and if nothing is entered, the location will be the same as the location of .lwi file.
        - %temp% folder is used for saving temporary files. They are deleted after the process is finished.
        - Sorting data is done with PowerShell, so you need have installed PowerShell.
    - For example, ~40 sec are needed for creating a final file from .lwi with 177000 frames. Less frames - faster process.
    - LibavInfo("file_path") - requires path to the new created file. It requires RT_Stats.
    - If one I frame is key frame, "(IDR)" is displayed.
- added LocateFrames.avsi
- added [MakeMultiPartScripts.avs](https://forum.doom9.org/showpost.php?p=1873782&postcount=1)
- added MatchFrames.avsi
- added [RT_Stats](https://forum.doom9.org/showthread.php?t=165479)
- added SirPlant.avsi
- updated TCannyMod - It seems the latest versions have a bug (the line at the bottom). Used 1.0.0, the latest version, unaffected.
- updated avsresize and avsresize.readme:
    - Added new option "dither" to z_...Resize functions. Values are the same as z_ConvertFormat(dither_type=). Default "none". For example:
        - z_Spline36Resize(x, y) = z_Spline36Resize(x, y, dither="none") - no dither
        - z_Spline36Resize(x, y, dither="error_diffusion") - used error_diffusion dither
- updated mvtools2
- added s_ExLogo.avsi

## plugins.pack.r4

- added [Deblock](https://github.com/mysteryx93/Avisynth-Deblock)
- added TTempSmooth
- updated avsresize:
    - zimg doesn't have Blackman, Spline64, Gauss, Sinc resize algos. Some functions were misleading/duplicated and they are removed - z_BlackmanResize (= z_LanczosResize), z_Spline64Resize (= z_Spline36Resize), z_GaussResize (= z_BicubicResize), z_SincResize (= z_Lanczos4Resize)
- updated avsresize.readme
- updated Bifrost:
    - Bifrost v.2 works on blocks and it has bug when the frame height isn't a multiple of the block height. Now again works on whole frame like v1.1 and it's actually ~3x faster
- added ExInpaint
- added rm_logo.avsi
- updated zlibDynamicTonemap.avsi
- updated FluxSmooth
- updated SupT.avsi[spoiler]Added new optional parameters (s_width, s_height, resizer(default Lanczos4Resize)) for scenarios when sup resolution is different than video resolution. s_width, s_height , resize are options of the sup file. Example for 4K video:
    - SupT("subtitle.sup") - 4K video and 4K resolution of the sup file
    - SupT("subtitle.sup", s_width=1920, s_height=1080) - 4K video and 1080p resolution of the sup file

## plugins.pack.r3

- updated zlibDynamicTonemap
- updated debandmask - high bit-depth and stack16_in/out support
- updated FixChromaBleedingMod - removed fast processing ("f") causing issues with non-even chroma size, high bit-depth support

## plugins.pack.r2

- added [TMM2](https://github.com/chikuzen/TMM2)
- updated Bifrost with faster build (~20% faster 64-bit version)(Didn't test 32-bit version)
- added modified [ChromaShiftSP2](https://forum.doom9.org/showpost.php?p=1851933&postcount=6) with high bit-depth support
- added modified [FixChromaBleedingMod](http://avisynth.nl/index.php/FixChromaBleedingMod) to work with ChromaShiftSP2

## plugins.pack.r1

- added DeDot - [vs port](https://github.com/dubhater/vapoursynth-dedot)
- added TComb - 32-bit version from [here](https://github.com/Elegant996/TComb), 64-bit version compiled from tritical original source because Elegant996 64-bit version have different output
- added TDeint
- updated zlibDynamicTonemap
- updated VariableBlur
- added SupT:
    - SupT(clip c, string filename, bool "forcedOnly", bool "swapCbCr", bool "relocate", string "relocOffset", int "radius", int "contrast")
    - script for [SupTitle](http://avisynth.nl/index.php?title=SupTitle) high bit-depth support
    - Inspired from [this post](https://forum.doom9.org/showpost.php?p=1866983&postcount=7)
    - The same usage as SupTitle except "radius" (default=4) which controls mask radius and "contrast" (default=128), both have no effect for 8-bit video
    - Support 8...32-bit video, requires SupTitle, dither. The options between "" are optional

## avs+ plugins update 04.04.2019

- changed Fixer radius (2) to max of left/top/right/bottom+2
- updated [FluxSmooth](https://github.com/pinterf/FluxSmooth/releases)
- added [debandmask](https://pastebin.com/SHQZjVJ5) (avs+ version fixed)
- added Nark..'s zlibDynamicTonemap
- added port of [FillBorders](https://github.com/dubhater/vapoursynth-fillborders):
    - usage: FillBorders(left, top, right, bottom, "mode")
    - "mode" is optional; default = FillMargins;
    - options for "mode" - 0 (FillMargins), 1 (Repeat), 2 (Mirror).
    - high bit-depth support
    - it's a bit faster and uses less memory than FillBorder

## avs+ plugins update 25.03.2019

- fixed avsresize parsing transfer function/primaries names when there is "-"; fixed zimg for Ryzen and Intel before Skylake CPUs.
- fixed AutoResize.avsi
- updated EdgeFixer for high bit-depth support
- added Fixer.avsi:
    - Alternative of FixRow(Column)Brightness/FillBorder/BalanceBorders/bbmod(BalanceBorders2)
    - Fixer(left, top, right, bottom, "radius", "cleft", "ctop", "cright", "cbottom", "cradius", "chroma"). The parameters between " " are optional and are referred to chroma planes.
    - EdgeFixer required. High bit-depth supported.
    - By default chroma=false and the filter is applied only to luma plane. When fixing only 1px on a side/top/bottom usually only luma plane is changed, so there is no point processing chroma planes.
    - When chroma=true, chroma planes are processed too. cleft, ctop, cright, cbottom, cradius can be omitted, default values should be ok for the most of cases. Default values of cleft, ctop, cright, cbottom are 0.5 of the equivalent luma parameters (rounded down).
    - Some examples:
        - Fixer(0,2,1,3) #only luma plane is processed
        - Fixer(0,2,1,3,chroma=true) #luma and chroma planes are processed for top, right side and bottom.
        - Fixer(0,2,1,3,chroma=true, cbottom=2) #same as above except on bottom 2px of chroma planes are filtered.

## avs+ plugins update 19.03.2019

- updated avsresize with two new options (nominal_luminance, approximate_gamma)
- added avsresize readme
- added Average
- added HDRTools
- added libfftw3f-3 for better compatibility
- added [EdgeFixer](https://github.com/sekrit-twc/EdgeFixer): ContinuityFixer(left, top, right, bottom); ReferenceFixer(clip ref, left, top, right, bottom, radius). It's kind of auto FixRow/ColumnBrightness.
