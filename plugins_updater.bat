@echo off

rem -------- Change defaults --------

rem The root location of the plugins that will be installed/updated:
set _default_location=C:\Program Files (x86)\AviSynth+

rem The name of the folder where the 64-bit plugins will be installed/updated:
set _folder_name64=plugins64+

rem Whether to include AiUpscale:
set _shaders=true

rem Whether to include crabshank's filters (https://github.com/crabshank/Avisynth-filters):
set _crabshank_filters=true

rem Whether to include LinearTransformation:
set linear_transf_=true

rem Whether to include AutoOverlay:
set autooverlay_=true

rem Whether to include CUDA only (for example DGDecodeNV.dll) plugins:
set cuda_only_=false

rem Whether to include Dogway_pack (https://github.com/Dogway/Avisynth-Scripts):
set dogway_pack_=true

rem Whether to include RIFE (https://github.com/Asd-g/AviSynthPlus-RIFE):
set rife_=true

rem Whether to include avs-mlrt (https://github.com/Asd-g/avs-mlrt):
set mlrt_=true

rem Whether to include w2xncnnvk (https://github.com/Asd-g/AviSynthPlus-w2xncnnvk):
set w2x_=true

rem -------- -------- -------- --------


set "_location64=%_default_location%\%_folder_name64%"


rem     setlocal enabledelayedexpansion
rem     for /r "%_default_location%\models_w2xncnnvk\" %%i in (*.*) do (
rem         set path_=%%i
rem         set path_parsed="%_location32%\!path_:%_default_location%\models_w2xncnnvk\=models\!"
rem         for %%j in (!path_parsed!) do (
rem             if not exist "%%~dpj" mkdir "%%~dpj"
rem             mklink %%j "%%i"
rem         )
rem     )
rem     endlocal


echo.
echo ****** move avs+ internal plugins to %temp% ******
echo.

mkdir "%temp%\64"

cd "%_location64%"

if exist colors_rgb.txt copy colors_rgb.txt "%temp%\64"
if exist ConvertStacked.dll copy ConvertStacked.dll "%temp%\64"
if exist DirectShowSource.dll copy DirectShowSource.dll "%temp%\64"
if exist ImageSeq.dll copy ImageSeq.dll "%temp%\64"
if exist Shibatch.dll copy Shibatch.dll "%temp%\64"
if exist TimeStretch.dll copy TimeStretch.dll "%temp%\64"
if exist VDubFilter.dll copy VDubFilter.dll "%temp%\64"
rem end ====================================


echo.
echo ****** clean plugins64+ folder ******
echo.

del /s /q *.*
rem end ====================================


cd "%_default_location%"


echo.
echo ****** clean plugins64+ parent folder ******
echo.

rem if remained from older versions
if exist "nnedi3_weights.bin" del "nnedi3_weights.bin"
rem end ====================================


echo.
echo ****** copy plugins_pack plugins ******
echo.

if exist plugs (
    git diff --name-only --quiet plugs "%~dp0plugins64+"

    if errorlevel 1 (
        rem reset errorlevel
        call;
        rd /s /q plugs
        robocopy "%~dp0plugins64+" "%_default_location%\plugs" /xj
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )
) else (
    robocopy "%~dp0plugins64+" "%_default_location%\plugs" /xj
    if errorlevel 8 (
        call :err
        goto :eof
    )
)

for %%i in ("%_default_location%\plugs\*.*") do (
    mklink "%_location64%\%%~nxi" "%%i"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%%i".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** move back avs+ internal plugins, delete temp folder in %temp% ******
echo.

move "%temp%\64\*.*" "%_location64%"
rd /q "%temp%\64"
rem end ====================================


echo.
echo ****** copy plugins_pack scripts ******
echo.

if exist scripts (
    git diff --name-only --quiet scripts "%~dp0scripts"

    if errorlevel 1 (
        rem reset errorlevel
        call;
        rd /s /q scripts
        robocopy "%~dp0scripts" "%_default_location%\scripts"
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )
) else (
    robocopy "%~dp0scripts" "%_default_location%\scripts"
    if errorlevel 8 (
        call :err
        goto :eof
    )
)

for %%i in ("%_default_location%\scripts\*.*") do (
    mklink "%_location64%\%%~nxi" "%%i"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%%i".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** make fftw3.dll symlink ******
echo.

if exist "%_location64%\libfftw3f-3.dll" mklink "%_location64%\fftw3.dll" "%_location64%\libfftw3f-3.dll"
rem end ====================================


echo.
echo ****** AutoOverlay ******
echo.

rem if remained from older versions
if exist aoverlay rd /s /q aoverlay

if "%autooverlay_%"=="true" (
    if exist AutoOverlay (
        git diff --name-only --quiet AutoOverlay "%~dp0AutoOverlay"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q AutoOverlay
            robocopy "%~dp0AutoOverlay" "%_default_location%\AutoOverlay" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0AutoOverlay" "%_default_location%\AutoOverlay" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    for %%i in ("%_default_location%\AutoOverlay\x64\*.*") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
    mklink "%_location64%\AutoOverlay_netautoload.dll" "%_default_location%\AutoOverlay\AutoOverlay_netautoload.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\AutoOverlay_netautoload.dll".
        goto :eof
    )
    mklink "%_location64%\MathNet.Numerics.dll" "%_default_location%\AutoOverlay\MathNet.Numerics.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\MathNet.Numerics.dll".
        goto :eof
    )
    mklink "%_location64%\OverlayUtils.avsi" "%_default_location%\AutoOverlay\OverlayUtils.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\OverlayUtils.avsi".
        goto :eof
    )
) else (
    if exist AutoOverlay rd /s /q AutoOverlay
)
rem end ====================================


echo.
echo ****** LinearTransformation ******
echo.

if "%linear_transf_%"=="true" (
    if exist LinearTransformation (
        git diff --name-only --quiet LinearTransformation\LUTs "%~dp0LinearTransformation\LUTs"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q LinearTransformation
            robocopy "%~dp0LinearTransformation" "%_default_location%\LinearTransformation" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        ) else (
            robocopy "%~dp0LinearTransformation" "%_default_location%\LinearTransformation" "*.avsi"
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0LinearTransformation" "%_default_location%\LinearTransformation" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    powershell -command "(gc 'LinearTransformation\LinearTransformation.avsi' -encoding "default") -replace [regex]::Escape('C:\Program Files (x86)\AviSynth+\LUTs'), '%_default_location%\LinearTransformation\LUTs' | out-file -encoding "default" 'LinearTransformation\LinearTransformation.avsi'"
    if errorlevel 1 (
        call :err
        echo Error LinearTransformation.avsi lut path changing.
        goto :eof
    )
    mklink "%_location64%\LinearTransformation.avsi" "%_default_location%\LinearTransformation\LinearTransformation.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\LinearTransformation.avsi".
        goto :eof
    )
) else (
    if exist LinearTransformation rd /s /q LinearTransformation
)
rem end ====================================


echo.
echo ****** AiUpscale ******
echo.

rem if remained from old versions
if exist Shaders rd /s /q Shaders

if "%_shaders%"=="true" (
    if exist AviSynthAiUpscale (
        git diff --name-only --quiet AviSynthAiUpscale "%~dp0AviSynthAiUpscale"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q AviSynthAiUpscale
            robocopy "%~dp0AviSynthAiUpscale" "%_default_location%\AviSynthAiUpscale" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )

            rd /s /q "%_location64%\Shaders"
        )
    ) else (
        robocopy "%~dp0AviSynthAiUpscale" "%_default_location%\AviSynthAiUpscale" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
        if exist "%_location64%\Shaders" rd /s /q "%_location64%\Shaders"
    )

    if not exist "%_location64%\Shaders" mklink /j "%_location64%\Shaders" "%~dp0AviSynthAiUpscale\Shaders"
    mklink "%_location64%\AiUpscale.avsi" "%_default_location%\AviSynthAiUpscale\AiUpscale.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\AiUpscale.avsi".
        goto :eof
    )
) else (
    if exist AviSynthAiUpscale rd /s /q AviSynthAiUpscale
    if exist "%_location64%\Shaders" rd /s /q "%_location64%\Shaders"
)
rem end ====================================


echo.
echo ****** Dogway_pack ******
echo.

if "%dogway_pack_%"=="true" (
    if exist Dogway_pack (
        git diff --name-only --quiet Dogway_pack "%~dp0Dogway_pack"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q Dogway_pack
            robocopy "%~dp0Dogway_pack" "%_default_location%\Dogway_pack" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0Dogway_pack" "%_default_location%\Dogway_pack" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    for %%i in ("%_default_location%\Dogway_pack\EX mods\*.avsi") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
    for %%i in ("%_default_location%\Dogway_pack\External_deps\x64\*.*") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
    for %%i in ("%_default_location%\Dogway_pack\MIX mods\*.avsi") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
    for %%i in ("%_default_location%\Dogway_pack\ex_SMDegrain\*.avsi") do (
    mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
    for %%i in ("%_default_location%\Dogway_pack\*.avsi") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
) else (
    if exist Dogway_pack rd /s /q Dogway_pack
)
rem end ====================================


echo.
echo ****** RIFE ******
echo.

rem if remained from old versions
if exist models_rife rd /s /q models_rife

if "%rife_%"=="true" (
    if not exist "%_location64%\models" mkdir "%_location64%\models"

    if exist rife (
        git diff --name-only --quiet rife\models "%~dp0rife\models"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q rife\models
            robocopy "%~dp0rife\models" "%_default_location%\rife\models" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet rife\x64 "%~dp0rife\x64"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q rife\x64
            robocopy "%~dp0rife\x64" "%_default_location%\rife\x64"
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0rife" "%_default_location%\rife" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    for /d %%i in ("%_location64%\models\*rife*") do if exist "%%i" rd /s /q "%%i"
    
    for /d %%i in ("%_default_location%\rife\models\*rife*") do (
        mklink /j "%_location64%\models\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%_location64%\RIFE.dll" "%_default_location%\rife\x64\RIFE.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\RIFE.dll".
        goto :eof
    )
) else (
    for /d %%i in ("%_location64%\models\*rife*") do if exist "%%i" rd /s /q "%%i"
    if exist rife rd /s /q rife
)
rem end ====================================


echo.
echo ****** mlrt ******
echo.

if "%mlrt_%"=="true" (
    if not exist "%_location64%\models" mkdir "%_location64%\models"
    if exist mlrt (
        git diff --name-only --quiet mlrt\models "%~dp0mlrt\models"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q mlrt\models
            robocopy "%~dp0mlrt\models" "%_default_location%\mlrt\models" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet mlrt\OpenVINO_runtimes "%~dp0mlrt\OpenVINO_runtimes"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q mlrt\OpenVINO_runtimes
            robocopy "%~dp0mlrt\OpenVINO_runtimes" "%_default_location%\mlrt\OpenVINO_runtimes" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet mlrt\x64 "%~dp0mlrt\x64"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q mlrt\x64
            robocopy "%~dp0mlrt\x64" "%_default_location%\mlrt\x64"
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet mlrt\mlrt.avsi "%~dp0mlrt\mlrt.avsi"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            del mlrt\mlrt.avsi
            robocopy "%~dp0mlrt" "%_default_location%\mlrt" mlrt.avsi
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet mlrt\mlrt_ov_loader.avsi "%~dp0mlrt\mlrt_ov_loader.avsi"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            del mlrt\mlrt_ov_loader.avsi
            robocopy "%~dp0mlrt" "%_default_location%\mlrt" mlrt_ov_loader.avsi
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0mlrt" "%_default_location%\mlrt" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    for /d %%i in ("%_default_location%\mlrt\models\*") do if exist "%_location64%\models\%%~nxi" rd /s /q "%_location64%\models\%%~nxi"

    for /d %%i in ("%_default_location%\mlrt\models\*") do (
        mklink /j "%_location64%\models\%%~nxi" "%_default_location%\mlrt\models\%%~nxi"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%_location64%\models\%%~nxi".
            goto :eof
        )
    )

    mklink "%_location64%\mlrt_ncnn.dll" "%_default_location%\mlrt\x64\mlrt_ncnn.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\mlrt_ncnn.dll".
        goto :eof
    )
    mklink "%_location64%\mlrt_ov.dll" "%_default_location%\mlrt\x64\mlrt_ov.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\mlrt_ov.dll".
        goto :eof
    )
    mklink "%_location64%\mlrt.avsi" "%_default_location%\mlrt\mlrt.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\mlrt.avsi".
        goto :eof
    )
    mklink "%_location64%\mlrt_ov_loader.avsi" "%_default_location%\mlrt\mlrt_ov_loader.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\mlrt_ov_loader.avsi".
        goto :eof
    )
) else (
    for /d %%i in ("%~dp0mlrt\models\*") do if exist "%_location64%\models\%%~nxi" rd /s /q "%_location64%\models\%%~nxi"
    if exist mlrt rd /s /q mlrt
)
rem end ====================================


echo.
echo ****** crabshank's filters ******
echo.

if "%_crabshank_filters%"=="true" (
    if exist crabshank_filters (
        git diff --name-only --quiet crabshank_filters "%~dp0crabshank_filters"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q crabshank_filters
            robocopy "%~dp0crabshank_filters" "%_default_location%\crabshank_filters" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0crabshank_filters" "%_default_location%\crabshank_filters" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )

    for %%i in ("%_default_location%\crabshank_filters\x64\*.*") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
) else (
    if exist "%_default_location%\crabshank_filters" rd /s /q "%_default_location%\crabshank_filters"
)
rem end ====================================


echo.
echo ****** CUDA only ******
echo.

if "%cuda_only_%"=="true" (
    robocopy "%~dp0cuda_only\x64" "%_default_location%\cuda_only\x64"
    if errorlevel 8 (
        call :err
        goto :eof
    )
    for %%i in ("%_default_location%\cuda_only\x64\*.*") do (
        mklink "%_location64%\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
) else (
    if exist "%_default_location%\cuda_only" rd /s /q "%_default_location%\cuda_only"
)
rem end ====================================


echo.
echo ****** w2xncnnvk ******
echo.

rem if remained from older versions
if exist model rd /s /q model
if exist models_w2xncnnvk rd /s /q models_w2xncnnvk

if "%w2x_%"=="true" (
    if not exist "%_location64%\models" mkdir "%_location64%\models"
    if exist w2xncnnvk (
        git diff --name-only --quiet w2xncnnvk\models "%~dp0w2xncnnvk\models"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q w2xncnnvk\models
            robocopy "%~dp0w2xncnnvk\models" "%_default_location%\w2xncnnvk\models" /s
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )

        git diff --name-only --quiet w2xncnnvk\x64 "%~dp0w2xncnnvk\x64"

        if errorlevel 1 (
            rem reset errorlevel
            call;
            rd /s /q w2xncnnvk\x64
            if errorlevel 1 (
                call :err
                goto :eof
            )
            robocopy "%~dp0w2xncnnvk\x64" "%_default_location%\w2xncnnvk\x64"
            if errorlevel 8 (
                call :err
                goto :eof
            )
        )
    ) else (
        robocopy "%~dp0w2xncnnvk" "%_default_location%\w2xncnnvk" /s
        if errorlevel 8 (
            call :err
            goto :eof
        )
    )
    
    for /d %%i in ("%_location64%\models\*models-*") do if exist "%%i" rd /s /q "%%i"

    for /d %%i in ("%_default_location%\w2xncnnvk\models\*models-*") do (
        mklink /j "%_location64%\models\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%_location64%\w2xncnnvk.dll" "%_default_location%\w2xncnnvk\x64\w2xncnnvk.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%_location64%\w2xncnnvk.dll".
        goto :eof
    )
) else (
    for /d %%i in ("%_location64%\models\*models-*") do if exist "%%i" rd /s /q "%%i"
    if exist w2xncnnvk rd /s /q w2xncnnvk
)
rem end ====================================


cd "%~dp0"
echo.
echo ****** Done ******
echo.
pause
goto :eof

:err
cd "%~dp0"
echo.
echo ****** ERROR! ******
echo.
pause
