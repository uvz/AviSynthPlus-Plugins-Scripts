@echo off

rem -------- Change defaults --------

rem Whether to include AiUpscale:
set _shaders=true

rem Whether to include crabshank's filters (https://github.com/crabshank/Avisynth-filters):
set _crabshank_filters=true

rem Whether to include LinearTransformation:
set linear_transf_=true

rem Whether to include AutoOverlay:
set autooverlay_=true

rem Whether to include CUDA only (for example DGDecodeNV.dll) plugins:
set cuda_only_=false

rem Whether to include Dogway_pack (https://github.com/Dogway/Avisynth-Scripts):
set dogway_pack_=true

rem Whether to include RIFE (https://github.com/Asd-g/AviSynthPlus-RIFE):
set rife_=true

rem Whether to include avs-mlrt (https://github.com/Asd-g/avs-mlrt):
set mlrt_=true

rem Whether to include w2xncnnvk (https://github.com/Asd-g/AviSynthPlus-w2xncnnvk):
set w2x_=true

rem -------- -------- -------- --------


echo.
echo ****** clean ******
echo.

rem del "%~dp0plugins64+\*.avs*"
rem
rem if exist "%~dp0plugins64+\models" rd /s /q "%~dp0plugins64+\models"
rem
rem for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.*") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem for %%i in ("%~dp0AutoOverlay\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\Shaders" rd /s /q "%~dp0plugins64+\Shaders"
rem
rem for %%i in ("%~dp0AutoOverlay\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem for %%i in ("%~dp0crabshank_filters\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\DGDecodeNV.dll" del "%~dp0plugins64+\DGDecodeNV.dll"
rem for %%i in ("%~dp0cuda_only\x64\*.dll") do if exist "%~dp0plugins64+\%%~nxi" del "%~dp0plugins64+\%%~nxi"
rem
rem if exist "%~dp0plugins64+\fftw3.dll" del "%~dp0plugins64+\fftw3.dll"
rem
rem if exist "%~dp0plugins64+\RIFE.dll" del "%~dp0plugins64+\RIFE.dll"
rem
rem if exist "%~dp0plugins64+\mlrt_ncnn.dll" del "%~dp0plugins64+\mlrt_ncnn.dll"

cd /d "%~dp0"
git clean -xfd
rem end ====================================


echo.
echo ****** create fftw3 symlinks ******
echo.

mklink "%~dp0plugins64+\fftw3.dll" "%~dp0plugins64+\libfftw3f-3.dll"
if errorlevel 1 (
    call :err
    echo Error during symlink creation "%~dp0plugins64+\fftw3.dll".
    goto :eof
)
rem end ====================================


echo.
echo ****** create scripts symlinks ******
echo.

for %%i in ("%~dp0\scripts\*.*") do (
    mklink "%~dp0plugins64+\%%~nxi" "%%i"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%%i".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** create AutoOverlay symlinks ******
echo.

if "%autooverlay_%"=="true" (
    for %%i in ("%~dp0AutoOverlay\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%~dp0plugins64+\OverlayUtils.avsi" "%~dp0AutoOverlay\OverlayUtils.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\OverlayUtils.avsi".
        goto :eof
    )

    for %%i in ("%~dp0AutoOverlay\x64\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
)
rem end ====================================


echo.
echo ****** create AiUpscale symlinks ******
echo.

if "%_shaders%"=="true" (
    mklink /j "%~dp0plugins64+\Shaders" "%~dp0AviSynthAiUpscale\Shaders"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\Shaders".
        goto :eof
    )
    mklink "%~dp0plugins64+\AiUpscale.avsi" "%~dp0AviSynthAiUpscale\AiUpscale.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\AiUpscale.avsi".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** create crabshank_filters symlinks ******
echo.

if "%_crabshank_filters%"=="true" (
    for %%i in ("%~dp0crabshank_filters\x64\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
)
rem end ====================================


echo.
echo ****** create LinearTransformation symlinks ******
echo.

if "%linear_transf_%"=="true" (
    robocopy "%~dp0LinearTransformation" "%~dp0plugins64+" "*.avsi"
    if errorlevel 8 (
        call :err
        goto :eof
    )
    powershell -command "(gc '%~dp0plugins64+\LinearTransformation.avsi' -encoding "default") -replace 'C:\\Program Files \(x86\)\\AviSynth\+\\LUTs\\', '%~dp0LinearTransformation\LUTs\' | out-file -encoding "default" '%~dp0plugins64+\LinearTransformation.avsi'"
    if errorlevel 1 (
        call :err
        echo Error LinearTransformation.avsi lut path changing.
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** create cuda_only symlinks ******
echo.

if "%cuda_only_%"=="true" (
    for %%i in ("%~dp0cuda_only\x64\*.dll") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
)
rem end ====================================


echo.
echo ****** create Dogway_pack symlinks ******
echo.

if "%dogway_pack_%"=="true" (
    for %%i in ("%~dp0Dogway_pack\External_deps\x64\*.*") do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    for /r "%~dp0Dogway_pack\" %%i in (*.avsi) do (
        mklink "%~dp0plugins64+\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )
)
rem end ====================================


echo.
echo ****** create RIFE symlinks ******
echo.

if "%rife_%"=="true" (
    if not exist "%~dp0plugins64+\models" mkdir "%~dp0plugins64+\models"
    for /d %%i in ("%~dp0rife\models\*") do (
        mklink /j "%~dp0plugins64+\models\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%~dp0plugins64+\RIFE.dll" "%~dp0rife\x64\RIFE.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\RIFE.dll".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** create mlrt symlinks ******
echo.

if "%mlrt_%"=="true" (
    if not exist "%~dp0plugins64+\models" mkdir "%~dp0plugins64+\models"
    for /d %%i in ("%~dp0mlrt\models\*") do (
        mklink /j "%~dp0plugins64+\models\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%~dp0plugins64+\mlrt_ncnn.dll" "%~dp0mlrt\x64\mlrt_ncnn.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\mlrt_ncnn.dll".
        goto :eof
    )
    mklink "%~dp0plugins64+\mlrt_ov.dll" "%~dp0mlrt\x64\mlrt_ov.dll"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\mlrt_ov.dll".
        goto :eof
    )
    mklink "%~dp0plugins64+\mlrt.avsi" "%~dp0mlrt\mlrt.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\mlrt_ov.dll".
        goto :eof
    )
    mklink "%~dp0plugins64+\mlrt_ov_loader.avsi" "%~dp0mlrt\mlrt_ov_loader.avsi"
    if errorlevel 1 (
        call :err
        echo Error during symlink creation "%~dp0plugins64+\mlrt_ov_loader.avsi".
        goto :eof
    )
)
rem end ====================================


echo.
echo ****** create w2xncnnvk symlinks ******
echo.

if "%w2x_%"=="true" (
    if not exist "%~dp0plugins64+\models" mkdir "%~dp0plugins64+\models"
    for /d %%i in ("%~dp0w2xncnnvk\models\*") do (
        mklink /j "%~dp0plugins64+\models\%%~nxi" "%%i"
        if errorlevel 1 (
            call :err
            echo Error during symlink creation "%%i".
            goto :eof
        )
    )

    mklink "%~dp0plugins64+\w2xncnnvk.dll" "%~dp0w2xncnnvk\x64\w2xncnnvk.dll"
)
rem end ====================================


echo.
echo ****** Done ******
echo.
pause
goto :eof

:err
echo.
echo ****** ERROR! ******
echo.
pause