## Notes

Open the scripts - usually the description and the usage info is in there.

## Category

[Anti-aliasing](#anti-aliasing)<br>
[Audio Filters](#audio-filters)<br>
[Averaging and Layering](#averaging-layering)<br>
[Blurring](#blurring)<br>
[Borders and Cropping](#borders-and-cropping)<br>
[Chroma correction](#chroma-correction)<br>
[Debanding](#debanding)<br>
[Deblocking](#deblocking)<br>
[Debugging and Diagnostic Filters](#debugging-diagnostic-filters)<br>
[Dehaloing](#dehaloing)<br>
[Deinterlacing](#deinterlacing)<br>
[Deringing and Mosquito Noise](#deringing-and-mosquito-noise)<br>
[Duplicate Frame Detectors](#duplicate-frame-detectors)<br>
[Edge Detection](#edge-detection)<br>
[Fieldblending and Frameblending removal](#fieldblending-and-frameblending-removal)<br>
[Film Damage correction](#film-damage-correction)<br>
[Frame Rate Conversion](#frame-rate-conversion)<br>
[Frame Replacement and Range Processing](#frame-replacement-and-range-processing)<br>
[Ghost Removal](#ghost-emoval)<br>
[Grain](#grain)<br>
[IVTC and Decimation](#ivtc-and-decimation)<br>
[Levels and Chroma](#levels-and-chroma)<br>
[Line Darkening](#line-darkening)<br>
[Logo Removal](#logo-removal)<br>
[Luma Equalization](#luma-equalization)<br>
[Masking](#masking)<br>
[Multipurpose Filters](#multipurpose-filters)<br>
[Rainbow and Dot Crawl Removal](#rainbow-and-dot-crawl-removal)<br>
[Resizers](#resizers)<br>
[Rotation and Shear and Skew and Perspective](#rotation-and-shear-and-skew-and-perspective)<br>
[Sharpening](#sharpening)<br>
[Source Filters](#source-filters)<br>
[Spatial Denoisers](#spatial-denoisers)<br>
[Spatio-Temporal Denoisers](#spatio-temporal-denoisers)<br>
[Stabilization](#stabilization)<br>
[Subtitling](#subtitling)<br>
[Support filters](#support-filters)<br>
[Temporal Denoisers](#temporal-denoisers)

### Anti-aliasing

Daa3.avsi<br>
[daa3MOD.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/daa3MOD.avsi)<br>
[HiAA_.avsi](https://github.com/AviSynth/avs-scripts/blob/master/HiAA.avsi)<br>
[MAA2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/maa2.avsi)<br>
[pow2Qt.avsi](https://forum.doom9.org/showthread.php?s=d828777f992605eda9266c521c5f1ac1&p=1737201#post1737201)<br>
[Santiag.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Santiag.avsi) (http://avisynth.nl/index.php/Santiag) <br>
[SantiagMod.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/SantiagMod.avsi) (optional Dogway_pack filter)

[Back to top](#category)

### Audio Filters

[AudioGraph.dll](https://github.com/Asd-g/AviSynth-AudioGraph)<br>
[SoxFilter.dll](https://github.com/pinterf/SoxFilter)<br>
[waveform.dll](http://avisynth.nl/index.php/Waveform)

[Back to top](#category)

### Averaging and Layering

[AutoOverlay](http://avisynth.nl/index.php/AutoOverlay) (optional filter)<br>
[Average.dll](http://avisynth.nl/index.php/Average)<br>
[ClipBlend.dll](http://avisynth.nl/index.php/ClipBlend)<br>
[insertsign.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/insertsign.avsi) (https://forums.animesuki.com/showthread.php?p=976257#post976257) <br>
[Logo.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/Logo.avsi) (optional Dogway_pack filter)<br>
[overlayplus.avsi](http://avisynth.nl/index.php/OverlayPlus)

[Back to top](#category)

### Blurring

[FastBlur.dll](http://avisynth.nl/index.php/FastBlur)<br>
[MedianBlur2.dll](http://avisynth.nl/index.php/MedianBlur2)<br>
[VariableBlur.dll](http://avisynth.nl/index.php/VariableBlur)

[Back to top](#category)

### Borders and Cropping

[AddBordersMod.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AddBordersMod.avsi)<br>
[bbmodY.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/bbmodY.avsi)<br>
[bborders.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/bborders.avsi)<br>
[Corners.avsi](https://forum.doom9.org/showthread.php?p=1819881#post1819881)<br>
[Drawrect_.avsi](https://forum.doom9.org/showthread.php?p=1531308)<br>
[EdgeFixer.dll](http://avisynth.nl/index.php/EdgeFixer)<br>
[FillBorders.dll](http://avisynth.nl/index.php/FillBorders)<br>
[FixBrightnessProtect3.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/FixBrightnessProtect3.avsi)<br>
[Fixer.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/Fixer.avsi)<br>
[Offset_video_.avsi](https://forum.doom9.org/showthread.php?t=178671)<br>
[RoboCrop.dll](http://avisynth.nl/index.php/RoboCrop)

[Back to top](#category)

### Chroma correction

[ChromaShiftSP2_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/ChromaShiftSP2_.avsi)<br>
[FixChromaBleedingMod_.avsi](http://avisynth.nl/index.php/FixChromaBleeding)

[Back to top](#category)

### Debanding

[F3KDB_s.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/F3KDB_s.avsi)<br>
[f3kpf.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/f3kpf.avsi)<br>
[flash3kyuu_deband.dll](http://avisynth.nl/index.php/F3kdb)<br>
[GradFun3DBmod.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/GradFun3DBmod.avsi)<br>
[GradFun3mod.avsi](https://pastebin.com/BY6CfBMv)<br>
[GradFun3plus.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/GradFun3plus.avsi) (optional Dogway_pack filter)<br>
[lfdeband.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/lfdeband.avsi)<br>
[neo_f3kdb.dll](http://avisynth.nl/index.php/Neo_f3kdb)

[Back to top](#category)

### Deblocking

[autodeblock2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/AutoDeblock2.avsi)<br>
[BlindPP.dll](https://www.rationalqm.us/dgmpgdec/DGDecodeManual.html#BlindPP)<br>
[Deblock.dll](http://avisynth.nl/index.php/DeBlock)<br>
[DeblockPack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/DeblockPack.avsi) (optional Dogway_pack filter)<br>
[Deblock_QED_MT2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Deblock_QED_MT2.avsi)<br>
[vsDeblockPP7.dll](http://avisynth.nl/index.php/VsDeblockPP7)

[Back to top](#category)

### Debugging and Diagnostic Filters

[BlockDetect.dll](https://github.com/Asd-g/AviSynthPlus-BlockDetect)<br>
[BlurDetect.dll](https://github.com/Asd-g/AviSynthPlus-BlurkDetect)<br>
[Butteraugli.dll](http://avisynth.nl/index.php/Butteraugli)<br>
[FFTSpectrum.dll](http://avisynth.nl/index.php/FFTSpectrum)<br>
[GMSD.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/GMSD.avsi)<br>
[MDSI.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/MDSI.avsi)<br>
[pixelscope.dll](http://avisynth.nl/index.php/Pixelscope)<br>
[SimilarityMetrics.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/SimilarityMetrics.avsi) (optional Dogway_pack filter)<br>
[ssimulacra.dll](https://github.com/Asd-g/AviSynthPlus-ssimulacra)<br>
[SysInfo.dll](https://forum.doom9.org/showthread.php?t=176131) (optional Dogway_pack filter)<br>
[VideoTek.avsi](http://forum.doom9.org/showthread.php?p=1832763#post1832763)<br>
[VMAF.dll](http://avisynth.nl/index.php/VMAF)<br>
[vsSSIM.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/vsSSIM.avsi)

[Back to top](#category)

### Dehaloing

[Abcxyz_MT2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Abcxyz_MT2.avsi)<br>
[BlindDeHalo3_mt2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/BlindDeHalo3_mt2.avsi)<br>
[DeHaloHmod.avsi](http://avisynth.nl/index.php/DeHaloHmod)<br>
[DeHalo_alpha_MT2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/DeHalo_alpha_mt2.avsi)<br>
[FineDehalo.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FineDehalo.avsi)<br>
[HaloBuster.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/HaloBuster.avsi)<br>
[Khameleon_.avsi](https://forum.doom9.org/showthread.php?t=174770)<br>
[Masked_DHA.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Masked_DHA.avsi)<br>
[VHSHaloremover.avsi](https://pastebin.com/u3B4WFT2)<br>
[YAHR.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/YAHRR.avsi)<br>
[YAHRmod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/YAHRmod.avsi)

[Back to top](#category)

### Deinterlacing

[BWDIF.dll](http://avisynth.nl/index.php/BWDIF)<br>
[EEDI2.dll](http://avisynth.nl/index.php/EEDI2)<br>
[EEDI2CUDA.dll](http://avisynth.nl/index.php/EEDI2CUDA) (optional cuda filter)<br>
[eedi3.dll](http://avisynth.nl/index.php/Eedi3)<br>
[EEDI3CL.dll](https://github.com/Asd-g/AviSynthPlus-EEDI3CL)<br>
[MCBob.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/MCBob.avsi)<br>
[nnedi3.dll](http://avisynth.nl/index.php/Nnedi3)<br>
[NNEDI3CL.dll](http://avisynth.nl/index.php/NNEDI3CL)<br>
[qCombed.dll](https://forum.doom9.org/showthread.php?t=173754)<br>
[QTGMC.avsi](http://avisynth.nl/index.php/QTGMC)<br>
[QTGMC+.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/QTGMC%2B.avsi) (optional Dogway_pack filter)<br>
[SangNom2.dll](http://avisynth.nl/index.php/SangNom2)<br>
[STGMC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/STGMC.avsi)<br>
[TDeint.dll](http://avisynth.nl/index.php/TDeint)<br>
[yadifmod2.dll](http://avisynth.nl/index.php/Yadifmod2)

[Back to top](#category)

### Deringing and Mosquito Noise

[EdgeCleaner.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/EdgeCleaner.avsi)<br>
[HQDeringmod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/HQDeringmod.avsi)<br>
[MosquitoNR.dll](http://avisynth.nl/index.php/MosquitoNR)<br>
[WarpDeRing.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/WarpDeRing.avsi)<br>
[WDFPlus.avsi](http://avisynth.nl/index.php/WDFPlus)

[Back to top](#category)

### Duplicate Frame Detectors

[DeDup.dll](http://avisynth.nl/index.php/DeDup)<br>
[DropDeadGorgeous.Avsi, DropDeadGorgeous_Client.Avs, Duplicity2.Avsi, Duplicity2_Client.avs](https://forum.doom9.org/showthread.php?t=175357)<br>
[ExactDeDup.dll](http://avisynth.nl/index.php/ExactDedup)

[Back to top](#category)

### Edge Detection

[AnimeMask.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AnimeMask.avsi)<br>
[AnimeMask2.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AnimeMask2.avsi)

[Back to top](#category)

### Fieldblending and Frameblending removal

[ExBlend.dll](https://forum.doom9.org/showthread.php?t=175948)<br>
[FixBlendIVTC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FixBlendIVTC.avsi)<br>
[Srestore.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Srestore.avsi) (http://avisynth.nl/index.php/Srestore)

[Back to top](#category)

### Film Damage correction

[DeScratch.dll](http://avisynth.nl/index.php/DeScratch)<br>
[DeSpot.dll](http://avisynth.nl/index.php/DeSpot)<br>
[RemoveDirt.dll](http://avisynth.nl/index.php/RemoveDirt)<br>
[RemoveDirtMC_SE.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/RemoveDirtMC_SE.avsi)

[Back to top](#category)

### Frame Rate Conversion

[FillMissing.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/FillMissing.avsi) (optional Dogway_pack filter)<br>
[FrameRateConverter.avsi, FrameRateConverter.dll](http://avisynth.nl/index.php/FrameRateConverter)<br>
[FrameRateConverterMIX.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/FrameRateConverterMIX.avsi) (optional Dogway_pack filter)<br>
[RIFE.dll](http://avisynth.nl/index.php/RIFE) (optional filter)<br>
[SmoothSkip.dll](https://forum.doom9.org/showthread.php?t=172377)<br>
[TimecodeFPS.dll](http://avisynth.nl/index.php/TimecodeFPS)

[Back to top](#category)

### Frame Replacement and Range Processing

[ClipClop.dll](http://avisynth.nl/index.php/ClipClop)<br>
[FrameSel.dll](http://avisynth.nl/index.php/FrameSel)<br>
[LocateFrames.avsi, MatchFrames.avsi](http://forum.doom9.org/showthread.php?t=164766)<br>
myselectrange.avsi<br>
[RemapFrames.dll](http://avisynth.nl/index.php/RemapFrames)<br>
[ReplaceFrameX.avsi](https://forum.doom9.org/showthread.php?p=1976553#post1976553)<br>
SirPlant.avsi

[Back to top](#category)

### Ghost Removal

[vsLGhost.dll](http://avisynth.nl/index.php/VsLGhost)

[Back to top](#category)

### Grain

[AddGrainC.dll](http://avisynth.nl/index.php/AddGrainC)<br>
[f3kgrainplus.avsi](http://avisynth.nl/index.php/F3kgrainPlus)<br>
[FilmGrain+.avsi](https://github.com/Dogway/Avisynth-Scripts/blob/master/FilmGrain%2B.avsi) (optional Dogway_pack filter)<br>
[GrainFactory3mod.avsi](https://gitlab.com/uvz/AVS-Stuff/-/blob/community-1/avs%202.5%20and%20up/GrainFactory3mod.avsi)<br>
[GrainStabilizeMC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/GrainStabilizeMC.avsi)<br>
[mRD_RestoreGrain.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/mRD_RestoreGrain.avsi)<br>
[sAddGrainCT.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/sAddGrainCT.avsi)

[Back to top](#category)

### IVTC and Decimation

[AnimeIVTC.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/AnimeIVTC.avsi)<br>
[AnimeIVTC_helper.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/AnimeIVTC_helper.avsi)<br>
[TIVTC.dll](http://avisynth.nl/index.php/TIVTC)

[Back to top](#category)

### Levels and Chroma

[ApplyGradationCurves.avsi](http://avisynth.nl/index.php/GradationCurve)<br>
[auto_gamma.dll](http://avisynth.nl/index.php/Auto_Gamma) (optional crabshank filter)<br>
[AutoLevels.dll](http://avisynth.nl/index.php/Autolevels)<br>
[Brightness.avsi](https://forum.doom9.org/showthread.php?p=1936224#post1936224)<br>
[bt2390_pq.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/bt2390_pq.avsi)<br>
[ColourWarp.dll](http://avisynth.nl/index.php/ColourWarp)<br>
[DoviBaker](https://github.com/erazortt/DoViBaker)<br>
[DGCube.dll](http://avisynth.nl/index.php/DGCube) (optional cuda filter)<br>
[DGHDRtoSDR.dll](http://avisynth.nl/index.php/DGHDRtoSDR)<br>
[DGPQtoHLG.dll](http://avisynth.nl/index.php/DGPQtoHLG)<br>
[DGTonemap.dll](http://avisynth.nl/index.php/DGTonemap)<br>
[edgelevel.dll](http://avisynth.nl/index.php/EdgeLevel)<br>
[gradation.dll](https://github.com/magiblot/gradation)<br>
[GradationCurve.dll](http://avisynth.nl/index.php/GradationCurve)<br>
[GradePack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/GradePack.avsi) (optional Dogway_pack filter)<br>
[grayworld.dll](https://github.com/Asd-g/AviSynthPlus-grayworld)<br>
[HDRTools.dll](http://avisynth.nl/index.php/HDRTools)<br>
[MapNLQ.dll](https://github.com/Asd-g/AviSynthPlus-MapNLQ)<br>
[MatchHistogram.dll](http://avisynth.nl/index.php/MatchHistogram)<br>
[linear_gamma.dll](https://github.com/crabshank/Avisynth-filters/blob/fb7ceebcf25dba5898f46b33dedeee63c059bb0f/C_Linear_Gamma_x64/linear_gamma.c#L220) (optional crabshank filter)<br>
[LinearTransformation.avsi](https://forum.doom9.org/showthread.php?t=176091) (optional filter)<br>
[MergeLMH_.avsi](https://forum.doom9.org/showthread.php?s=3aa23a52382ef3b41c24eb0ad558ab91&p=1691736#post1691736)<br>
[oSmoothLevels_.avsi](https://gitlab.com/uvz/AVS-Stuff/-/blob/community-1/avs%202.5%20and%20up/oSmoothLevels_.avsi)<br>
[Retinex.dll](https://github.com/Asd-g/AviSynth-Retinex)<br>
[saturation_percentiles.dll](https://github.com/crabshank/Avisynth-filters/blob/master/C_Saturation_percentiles_x64/Function%20definition.txt) (optional crabshank filter)
[SmoothAdjust.dll](http://avisynth.nl/index.php/SmoothAdjust)<br>
[SoftLight.dll](https://forum.doom9.org/showthread.php?t=184943)<br>
[Tonemapper.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/Tonemapper.avsi)<br>
[TransformsPack - Main.avsi, TransformsPack - Models.avsi, TransformsPack - Transfers.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/tree/patched) (optional Dogway_pack filter)<br>
[vscube.dll](http://avisynth.nl/index.php/AVSCube)<br>
[white_point.dll](http://avisynth.nl/index.php/Manual_WP) (optional crabshank filter)<br>
[YLevels_mt_.avsi](http://avisynth.nl/index.php/Ylevels)<br>
zlibDynamicTonemap.avsi

[Back to top](#category)

### Line Darkening

[FastLineDarken.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FastLineDarkMOD2.avsi)<br>
[FastLineDarkenPlus.avsi](https://github.com/Dogway/Avisynth-Scripts/blob/master/EX%20mods/FastLineDarkenPlus.avsi) (optional Dogway_pack filter)<br>
[Hysteria.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Hysteria.avsi)<br>
Toon.avsi

[Back to top](#category)

### Logo Removal

[AvsInPaint.dll](http://avisynth.nl/index.php/AvsInpaint)<br>
[DoomDelogo.avsi](https://github.com/Purfview/DoomDelogo)<br>
[exinpaint.dll](http://avisynth.nl/index.php/ExInpaint)<br>
[InpaintDelogo.avsi](https://forum.doom9.org/showthread.php?t=176860)<br>
[InpaintFunc.avsi](http://avisynth.nl/index.php/InpaintFunc)<br>
[rm_logo_.avsi](http://avisynth.nl/index.php/Rm_logo)<br>
[s_ExLogo.avsi](http://forum.doom9.org/showthread.php?t=154559)

[Back to top](#category)

### Luma Equalization

[Deflicker.dll](http://avisynth.nl/index.php/DeFlicker)<br>
[ReduceFlicker.dll](http://avisynth.nl/index.php/ReduceFlicker)<br>
[Small_Deflicker.avsi](https://forum.doom9.org/showthread.php?p=1812060#post1812060)<br>
[vinverse.dll](http://avisynth.nl/index.php/Vinverse)

[Back to top](#category)

### Masking

[AGM.dll](http://avisynth.nl/index.php/AGM)<br>
[CombMask.dll](http://avisynth.nl/index.php/CombMask)<br>
[debandmask_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/debandmask_.avsi)<br>
[MasksPack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MasksPack.avsi) (optional Dogway_pack filter)<br>
[MTCombMask.dll](http://avisynth.nl/index.php/MTCombMask)<br>
[mt_xxpand_multi.avsi](http://avisynth.nl/index.php/Dither)<br>
[retinex_edgemask.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/retinex_edgemask.avsi)<br>
[solarCurve.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/solarCurve.avsi)<br>
[tcolormask.dll](http://avisynth.nl/index.php/TColorMask)<br>
[tmaskcleaner.dll](http://avisynth.nl/index.php/TMaskCleaner)<br>
[TMM2.dll](http://avisynth.nl/index.php/TMM2)<br>
[vsTEdgeMask.dll](http://avisynth.nl/index.php/VsTEdgeMask)<br>
[vsTMM](https://github.com/Asd-g/AviSynth-vsTMM)

[Back to top](#category)

### Multipurpose Filters

[avs_libplacebo.dll](http://avisynth.nl/index.php/Avslibplacebo)<br>
[avsresize.dll](http://avisynth.nl/index.php/Avsresize)<br>
[dither.avsi, dither.dll](http://avisynth.nl/index.php/Dither_tools)<br>
[ExTools.avsi](https://github.com/Dogway/Avisynth-Scripts/blob/master/ExTools.avsi)<br>
[fmtconv.dll](http://avisynth.nl/index.php/Fmtconv)<br>
[manyPlus.dll](http://avisynth.nl/index.php/ManyPlus)<br>
[masktools2.dll](http://avisynth.nl/index.php/MaskTools2)<br>
[mlrt.avsi](https://github.com/Asd-g/avs-mlrt/blob/main/mlrt.avsi)<br>
[mlrt_ncnn.dll](https://github.com/Asd-g/avs-mlrt)<br>
[mlrt_ov.dll](https://github.com/Asd-g/avs-mlrt)<br>
[mvtools2.dll](http://avisynth.nl/index.php/MVTools)<br>
[Shader.avsi, Shader.dll](http://avisynth.nl/index.php/AviSynthShader)<br>
[vsTCanny.dll](http://avisynth.nl/index.php/VsTCanny)

[Back to top](#category)

### Rainbow and Dot Crawl Removal

[ASTDR.avsi](http://avisynth.nl/index.php/ASTDR)<br>
[bifrost.dll](http://avisynth.nl/index.php/Bifrost)<br>
[checkmate.dll](http://avisynth.nl/index.php/Checkmate)<br>
[ChubbyRain2_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/ChubbyRain2_.avsi)<br>
DDComb_.avsi<br>
[ddcr.avsi](http://avisynth.nl/index.php/DDCR)<br>
[DeCross.dll](http://avisynth.nl/index.php/DeCross)<br>
[DeDot.dll](http://avisynth.nl/index.php/DeDot)<br>
[DFMDeRainbow.avsi](http://avisynth.nl/index.php/DFMDeRainbow)<br>
[DotKill.dll](http://avisynth.nl/index.php/DotKill)<br>
[LUTDeCrawl.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LUTDeCrawl.avsi)<br>
[LUTDeRainbow.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LUTDeRainbow.avsi)<br>
[rainbow_smooth2.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/rainbow_smooth2.avsi)<br>
[TComb.dll](http://avisynth.nl/index.php/TComb)<br>
[unb_s.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/unb_s.avsi) (https://forum.doom9.org/showthread.php?p=1379930&highlight=function#post1379930)

[Back to top](#category)

### Resizers

[AiUpscale](https://forum.doom9.org/showthread.php?t=181665) (optional filter)<br>
[AreaResize.dll](http://avisynth.nl/index.php/AreaResize) (optional Dogway_pack filter)<br>
[AutoResize_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/AutoResize_.avsi)<br>
[ChromaReconstructorMod.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/ChromaReconstructorMod.avsi) (optional Dogway_pack filter)<br>
[DebilinearM_.avsi](https://gitlab.com/uvz/AVS-Stuff/-/blob/community-1/avs%202.6%20and%20up/DebilinearM_.avsi)<br>
[DPID.dll](http://avisynth.nl/index.php/DPID)<br>
[edi_rpow2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/edi_rpow2.avsi)<br>
[eedi3_resize.avsi](https://github.com/AviSynth/avs-scripts/blob/master/eedi3_resize.avsi)<br>
[eedi3_resize16.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/eedi3_resize16.avsi)<br>
[FCBI.dll](http://avisynth.nl/index.php/FCBI) (optional Dogway_pack filter)<br>
[IQA_downsample.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/IQA_downsample.avsi)<br>
[IResize.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/IResize.avsi)<br>
[JincResize.dll](http://avisynth.nl/index.php/JincResize)<br>
[libdescale.dll](https://github.com/Irrational-Encoding-Wizardry/descale/)<br>
[MultiSWAR_V2.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/MultiSWAR_V2.avsi) (optional Dogway_pack filter)<br>
[nnchromaupsubsampling.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/nnchromaupsubsampling.avsi)<br>
[nnedi3_resize16.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/nnedi3_resize16.avsi)<br>
[NNEDI3CL_rpow2.avsi](https://github.com/Asd-g/AviSynthPlus-NNEDI3CL/blob/main/NNEDI3CL_rpow2.avsi)<br>
[ResampleMT.dll](http://avisynth.nl/index.php/ResampleMT)<br>
[Resize8.avsi](https://forum.doom9.org/showthread.php?t=183057) (http://avisynth.nl/index.php/Resize8) <br>
[ResizersPack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/ResizersPack.avsi) (optional Dogway_pack filter)<br>
[ResizeX.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/ResizeX.avsi)<br>
[scaled444tonative.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/scaled444tonative.avsi)<br>
[spline36resizemod_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/spline36resizemod_.avsi)<br>
[SSIM_downsample.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/SSIM_downsample.avsi)<br>
[ssubsampling_convert.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.6%20and%20up/ssubsampling_convert.avsi)<br>
[w2xncnnvk.dll](http://avisynth.nl/index.php/W2xncnnvk)

[Back to top](#category)

### Rotation and Shear and Skew and Perspective

[circle_warp.dll](https://github.com/crabshank/Avisynth-filters/blob/master/C_Circle_Warp_x64/Function%20definition) (optional crabshank filter)<br>
[Rotate.dll](http://avisynth.nl/index.php/Rotate)<br>
[warp.dll](http://avisynth.nl/index.php/Warp)

[Back to top](#category)

### Sharpening

[ASharp.dll](http://avisynth.nl/index.php/ASharp)<br>
[aWarpSharp4xx.avsi](https://pastebin.com/qLkBTbiF)<br>
[aWarpsharpMT.dll](http://avisynth.nl/index.php/AWarpSharp2)<br>
[CAS.dll](http://avisynth.nl/index.php/CAS)<br>
[ContraSharpen_mod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/ContraSharpen_mod.avsi)<br>
ContraSharpen_mod_16_1.3_uMod_.avsi<br>
[DDSharp.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/DDSharp.avsi)<br>
[FineSharp.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/FineSharp.avsi)<br>
[LimitedSharpen2.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LimitedSharpen2.avsi)<br>
[LSFmod.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LSFmod.avsi)<br>
[LSFplus.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/LSFplus.avsi) (optional Dogway_pack filter)<br>
[LSharpAAF.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/LSharpAAF.avsi) (optional Dogway_pack filter)<br>
[MCaWarpSharp4.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/MCaWarpSharp4.avsi) (optional Dogway_pack filter)<br>
[pSharpen.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/pSharpen.avsi) (http://avisynth.nl/index.php/PSharpen) <br>
[SeeSaw.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/seesaw.avsi) (http://avisynth.nl/index.php/SeeSaw) <br>
[SharpenersPack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/SharpenersPack.avsi) (optional Dogway_pack filter)<br>
[Soothe.avsi](http://avisynth.nl/index.php/Soothe)<br>
[TUnsharp.dll](http://avisynth.nl/index.php/TUnsharp)<br>
[UnsharpHQmod.dll](http://avisynth.nl/index.php/UnsharpHQ)<br>
[UnsharpMask_avsi.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/UnsharpMask_avsi.avsi) (http://avisynth.nl/index.php/WarpSharp/UnsharpMask) <br>
[vsMSharpen.dll](http://avisynth.nl/index.php/VsMSharpen)<br>
[warpsharp.dll](http://avisynth.nl/index.php/WarpSharp) (optional Dogway_pack filter)<br>
[Xsharpen_avsi.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Xsharpen_avsi.avsi) (http://avisynth.nl/index.php/WarpSharp/Xsharpen)

[Back to top](#category)

### Source Filters

[BestAudioSource.dll](http://avisynth.nl/index.php/BestAudioSource)<br>
[D2VSource.dll](http://avisynth.nl/index.php/MPEG2DecPlus)<br>
[DGDecodeNV.dll](https://www.rationalqm.us/dgdecnv/dgdecnv.html) (optional cuda filter)<br>
[ffms2.dll](http://avisynth.nl/index.php/FFmpegSource)<br>
[LSMASHSource.dll](http://avisynth.nl/index.php/LSMASHSource)<br>
[RawSourcePlus.dll](http://avisynth.nl/index.php/RawSource26)

[Back to top](#category)

### Spatial Denoisers

[Bilateral.dll](https://github.com/Asd-g/AviSynth-Bilateral)<br>
[DCTFilter.dll](http://avisynth.nl/index.php/DctFilter)<br>
[DeStripe.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/EX%20mods/DeStripe.avsi) (optional Dogway_pack filter)<br>
[ex_Spresso.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/MIX%20mods/ex_Spresso.avsi) (optional Dogway_pack filter)<br>
[Frfun7.dll](http://avisynth.nl/index.php/Frfun7)<br>
[luma_smooth.dll](https://github.com/crabshank/Avisynth-filters/blob/master/C_Luma_Smooth_x64/Function_definition.txt) (optional crabshank filter)<br>
[neo_minideen.dll](http://avisynth.nl/index.php/MiniDeen)<br>
[neo_vague_denoiser.dll](http://avisynth.nl/index.php/Neo_VagueDenoiser)<br>
[sbr.dll](http://avisynth.nl/index.php/Sbr)<br>
[SmoothUV2.dll](http://avisynth.nl/index.php/SmoothUV2)<br>
[Spresso.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Spresso.avsi) (http://avisynth.nl/index.php/SPresso) <br>
[vsMSmooth.dll](http://avisynth.nl/index.php/VsMSmooth)<br>
[vsTBilateral.dll](http://avisynth.nl/index.php/VsTBilateral)<br>
[yugefunc.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/yugefunc.avsi) (optional Dogway_pack filter)

[Back to top](#category)

### Spatio-Temporal Denoisers

[Advanced_Denoising.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/AdvancedDenoising.avsi)<br>
[BM3DCUDA_AVS.dll](http://avisynth.nl/index.php/BM3DCUDA) (optional cuda filter)<br>
[BM3DCPU_AVS.dll, BM3D_VAggregate](http://avisynth.nl/index.php/BM3DCUDA)<br>
[Convolution3D.dll](http://avisynth.nl/index.php/Convolution3D)<br>
[DenoiseSelective.avsi](https://forum.doom9.org/showthread.php?p=1914252#post1914252)<br>
[dfttest.dll](http://avisynth.nl/index.php/Dfttest)<br>
[dfttestMC_and_MC_things.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/dfttestMC_and_MC_things.avsi)<br>
[ex_SMDegrain.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/ex_SMDegrain/ex_SMDegrain.avsi) (https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/ex_SMDegrain/SMDegrain%20v3.5.4d.html) (optional Dogway_pack filter)<br>
[fft3dfilter.dll](http://avisynth.nl/index.php/FFT3DFilter)<br>
[FluxSmooth.dll](http://avisynth.nl/index.php/FluxSmooth)<br>
[hqdn3d.dll](http://avisynth.nl/index.php/Hqdn3d)<br>
[KNLMeansCL.dll](http://avisynth.nl/index.php/KNLMeansCL)<br>
[LRemoveDust.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/LRemoveDust.avsi)<br>
[MCDegrain.avsi, MCDegrainSharp.avsi](https://forum.doom9.org/showthread.php?p=1737045#post1737045)<br>
[mClean_.avsi](https://forum.doom9.org/showthread.php?p=1942879#post1942879)<br>
[MCTD_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/MCTD_.avsi)<br>
[neo_dfttest.dll](https://github.com/HomeOfAviSynthPlusEvolution/neo_DFTTest)<br>
[neo_fft3d.dll](https://github.com/HomeOfAviSynthPlusEvolution/neo_FFT3D)<br>
[RemoveGrainHD.dll](http://avisynth.nl/index.php/RemoveGrainHD)<br>
[RgTools.dll](http://avisynth.nl/index.php/RgTools)<br>
[SMDegrain_.avsi](https://gitlab.com/uvz/AVS-Stuff/-/blob/community-1/avs%202.5%20and%20up/SMDegrain_.avsi) (http://avisynth.nl/index.php/SMDegrain) <br>
[STpresso.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/STpresso.avsi)<br>
[TNLMeans.dll](http://avisynth.nl/index.php/TNLMeans)<br>
[vsDeGrainMedian.dll](http://avisynth.nl/index.php/VsDeGrainMedian)<br>
[VSmClean.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/Others/VSmClean.avsi)

[Back to top](#category)

### Stabilization

[DePan.dll](http://avisynth.nl/index.php/DePan)<br>
[DePanEstimate.dll](http://avisynth.nl/index.php/DePanEstimate)<br>
[Stabilization Tools Pack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/Stabilization%20Tools%20Pack.avsi) (optional Dogway_pack filter)

[Back to top](#category)

### Subtitling

[assrender.dll](http://avisynth.nl/index.php/AssRender)<br>
[BlendSubtitles.avsi](https://github.com/Asd-g/AviSynthPlus-SubImageFile/blob/main/BlendSubtitles.avsi)<br>
[SubImageFile.dll](https://github.com/Asd-g/AviSynthPlus-SubImageFile)<br>
[VSFilter.dll](http://avisynth.nl/index.php/Xy-VSFilter)

[Back to top](#category)

### Support filters

autoLoadDLLs.avsi<br>
AviSynthPluginsDir.avsi<br>
[CallCmd.dll](http://avisynth.nl/index.php/CallCmd)<br>
[ComparisonGen.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/ComparisonGen.avsi)<br>
[ClipBoard.dll](http://avisynth.nl/index.php/ClipBoard) (optional Dogway_pack filter)<br>
Delay_.avsi<br>
[dldet.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/dldet.avsi)<br>
[Estimate_Nnedi3_Rpow2.avsi](https://forum.doom9.org/showthread.php?t=176437)<br>
[FFMS2_.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/FFMS2_.avsi)<br>
[FFMS_MakeMultiPartScripts.avsi, FFMS_MakeMultiPartScripts_Client.avs](https://forum.doom9.org/showpost.php?p=1873783&postcount=2]FFMS_MakeMultiPartScripts.avsi)<br>
[grunt.dll](http://avisynth.nl/index.php/GRunT)<br>
[LinesLumaDiff.dll](http://avisynth.nl/index.php/LinesLumaDiff)<br>
[LoadDLL.dll](http://avisynth.nl/index.php/LoadDLL)<br>
[LUtils.avsi](https://github.com/AviSynth/avs-scripts/blob/master/LUtils.avsi)<br>
[LWLInfo.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/LWLInfo.avsi)<br>
[MakeMultiPartScripts.avs](https://forum.doom9.org/showthread.php?p=1870427#post1870427)<br>
[MaxCLLFindAVS.dll](https://github.com/erazortt/MaxCLLFindAVS)<br>
mlrt_ov_loader.avsi<br>
[MP_Pipeline.dll, MP_Pipeline.dll.slave.exe, MP_Pipeline.dll.win32, MP_Pipeline.dll.win32.slave.exe](http://avisynth.nl/index.php/MP_Pipeline)<br>
mtmodes.avsi<br>
myffinfo.avsi<br>
MyLWLInfo.avsi<br>
[PropToClip.dll](https://github.com/Asd-g/AviSynthPlus-PropToClip)<br>
[rgb_dither.dll](https://github.com/crabshank/Avisynth-filters/blob/master/C_RGB_Dither_x64/Function_definition.txt) (optional crabshank filter)<br>
[RoundHalfToEven.avsi](https://github.com/Asd-g/AviSynthPlus-Scripts/blob/master/RoundHalfToEven.avsi)<br>
[RT_Stats.dll](http://avisynth.nl/index.php/RT_Stats)<br>
UpscaleCheck.avsi<br>
[ScenesPack.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/ScenesPack.avsi) (optional Dogway_pack filter)<br>
[SwipeSubs.avsi](https://gitlab.com/uvz/Avisynth-Scripts/-/blob/patched/SwipeSubs.avsi) (optional Dogway_pack filter)<br>
[Zs_RF_Shared.avsi](https://github.com/realfinder/AVS-Stuff/blob/Community/avs%202.5%20and%20up/Zs_RF_Shared.avsi)

[Back to top](#category)

### Temporal Denoisers

[neo_tmedian.dll](https://github.com/HomeOfAviSynthPlusEvolution/neo_TMedian)<br>
[TemporalDegrain2.avsi](http://avisynth.nl/index.php/TemporalDegrain2)<br>
[TemporalDegrain2_fast.avsi](https://forum.doom9.org/showthread.php?p=1982594#post1982594)<br>
[vsCnr2.dll](http://avisynth.nl/index.php/VsCnr2)<br>
[vsTTempSmooth.dll](http://avisynth.nl/index.php/VsTTempSmooth)

[Back to top](#category)
